import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h2')).getText();
  }


  getLoginSubmitButton() {
    return element(by.name('submit_login'));
  }

}
