import { AppPage } from './app.po';
import {browser, by, element, ExpectedConditions} from 'protractor';
import {fail} from "jasmine";

describe('Filigranes App', () => {
  let page: AppPage;

  const EC = ExpectedConditions;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display title website', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Filigranes Pour Tous');
  });


  it('should not allow login submit without fields completed', () => {
    page.navigateTo();
    browser.wait(EC.elementToBeClickable(page.getLoginSubmitButton()), 1000).then(function() {
      console.log('error element clickable');
      fail();
    }, function(err) {
      console.log('ok, element not clickable' + err);
    });
  });
});
