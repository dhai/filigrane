import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FiligraneComponent} from './filigrane.component';

describe('FiligraneComponent', () => {
  let component: FiligraneComponent;
  let fixture: ComponentFixture<FiligraneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FiligraneComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiligraneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
