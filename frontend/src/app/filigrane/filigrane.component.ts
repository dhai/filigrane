'use strict';

import {Component, OnInit, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {DocumentListService} from '../document-list.service';
import {ConfirmDialogDirective} from '../ConfirmDialog';

import {environment} from '../../environments/environment';

import {SwPush} from '@angular/service-worker';

import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-filigrane',
  templateUrl: './filigrane.component.html',
  styleUrls: ['./filigrane.component.scss'],
  providers: [] // this depends on situation, see below
})

export class FiligraneComponent implements OnInit, OnDestroy {
  documents: any;
  selected = [];
  rows: any;

  // username of the person loggedin
  userName = "";

  // show/hide spinner while loading data
  showSpinner = false;

  // UI language setup
  browserLangage = 'en';
  isLangInit = false;

  // for notification
  message = "";

  //
  constructor(private router: Router,
              private http: HttpClient,
              private swPush: SwPush,
              private documentListService: DocumentListService,
              private translate: TranslateService) {
    // cosntructor code
  }

  //
  ngOnInit() {
    // disable authentication requirement
    // if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
    //  this.router.navigate(['login']);
    // } else {
    this.userName = localStorage.getItem('fptLogin');

    // set the http header with the jwt token in the global storage
    const httpOptions = {
      // no authentication
      // headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
    };

    //  load/relaod service list if not available.
    if (!this.documentListService.isInit()) {
      // display spinner while loading data
      this.showSpinner = true;
      this.http.get(environment.backendServerUrl + '/filigrane', httpOptions)
        .subscribe(data => {
            // for the ng angular table
            this.documents = data;
            // for ngx-datatables
            this.documentListService.addList(data);
            this.rows = data;

            // completed, hides spinner
            this.showSpinner = false;
          }, err => {
            console.log('[FiligraneComponent] http.get error', JSON.stringify(err));
            this.showSpinner = false;
            if (err.status === 401) {
              this.router.navigate(['login']);
            }
          }
        );
    } else {
      // directly use global list
      this.rows = this.documentListService.getList();
    }
    // }

    // TODO remove - test code for checking in console that notification can be received
    this.swPush.messages.subscribe(message => {
      console.log('[App] Push message received', message)
    });

    // TODO remove - test code for checking offline/online in console
    if (navigator.onLine === false) {
      // console.log("OFFLINE");
    } else {
      // console.log("ONLINE");
    }
  }

  //
  ngOnDestroy() {
  }

  //
  ngAfterViewInit() {
  }

  //
  // datatable buttons/actions
  //

  /**
   * Redirects the app to the details page
   *
   * @param documentId
   */
  detailsItem(documentId: any) {
    // console.log('view details of document ' + documentId);
    this.router.navigate(['/filigrane-details', documentId], {state: {isMatchDetails: false}});
  }

  /**
   *
   */
  cancel() {
  }

  /**
   * Process the request to delete a document.
   * Assumes that a confirmation has already been requested to the user.
   *
   * @param documentId
   */
  deleteItem(documentId) {
    if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
      this.router.navigate(['login']);
    } else {
      for (let tmpItem of this.rows) {
        if (tmpItem.documentId === documentId) {
          if (tmpItem.userId === this.userName) {
            // set the http header with the jwt token in the global storage
            const httpOptions = {
              headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
            };
            this.showSpinner = true;
            this.http.delete(environment.backendServerUrl + '/filigrane/' + documentId, httpOptions)
              .subscribe(res => {
                  this.reloadList();
                }, (err) => {
                  this.showSpinner = false;
                  console.log(err);
                }
              );
          }
        }
      }
    }
  }

  /**
   * Checks that the current user is the creator of the document
   * Used to display or not the delete button
   *
   * @param documentId
   * @returns {boolean}
   */
  idUserContribution(documentId: any) {
    for (let tmpItem of this.rows) {
      if (tmpItem.documentId === documentId) {
        return (tmpItem.userId === this.userName);
      }
    }
    return false;
  }

  //
  //
  //

  /**
   * Reloads the list of available documents from the server.
   * This is in response to the reload button press.
   * This call will update also the list stored in the document service
   *
   */
  reloadList() {
    // set the http header with the jwt token in the global storage
    const httpOptions = {
      headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
    };

    // should disable other buttons until completed to avoid clicking multiple times
    this.showSpinner = true;
    this.http.get(environment.backendServerUrl + '/filigrane', httpOptions).subscribe(data => {
      // for the ng angular table
      this.documents = data;
      // for ngx-datatables
      this.documentListService.addList(data);
      this.rows = data;
      // completed, hides spinner
      this.showSpinner = false;
    });
  }

  //
  // selection checkbo operations
  //

  add() {
    this.selected.push(this.rows[1], this.rows[3]);
  }

  update() {
    this.selected = [this.rows[1], this.rows[3]];
  }

  remove() {
    this.selected = [];
  }

  onSelect({selected}) {
    // console.log('Select Event', selected, this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  // mouse over on datatable item
  onActivate({selected}) {
    // console.log('Activate Event', selected, this.selected);
  }

  // determines if the checkbox should be displayed or not (is the element checkable)
  displayCheck(row) {
    // always true for now
    return true;
  }

  //
  //
  //

  isConnected() {
    return this.documentListService.isUserConnected();
  }


}
