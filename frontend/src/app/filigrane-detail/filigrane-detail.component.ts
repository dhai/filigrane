'use strict';

import {Component, OnInit, OnDestroy, ViewEncapsulation, HostListener, ChangeDetectorRef} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

import {DocumentListService} from '../document-list.service';

import {DocumentClass} from '../DocumentClass';
import {ThumbnailClass} from '../DocumentClass';
import {PageClass} from '../DocumentClass';
import {ImageClass} from '../ImageClass';

import {environment} from '../../environments/environment';

import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-filigrane-detail',
  templateUrl: './filigrane-detail.component.html',
  styleUrls: ['./filigrane-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FiligraneDetailComponent implements OnInit, OnDestroy {

  // show/hide spinner while loading data
  showSpinner = false;

  document: DocumentClass = new DocumentClass();
  userName = "";

  images: string[] = [];

  isMatchDetails = false;

  // UI language setup
  browserLangage = 'en';
  isLangInit = false;


  doShowFS = false;
  fsImageId = "";

  myThumbnail = "";
  myFullresImage = "";

  public innerWidth: any;

  //
  //
  //

  constructor(private router: Router,
              private route: ActivatedRoute,
              private http: HttpClient,
              private documentListService: DocumentListService,
              private translate: TranslateService,
              private cdRef: ChangeDetectorRef) {
    // cosntructor code
    try {
      // extras in router navigation need to be extracted from the constructor, not in ngOnInit (too late)
      this.isMatchDetails = router.getCurrentNavigation().extras.state.isMatchDetails;
    } catch (e) {
      this.isMatchDetails = false;
    }

  }

  //
  //
  ngOnInit() {
    // get and display the detals for the selected document
    this.getDocument(this.route.snapshot.params['id']);

    // initialize user name
    this.userName = localStorage.getItem('fptLogin');
    if (this.userName === null || this.userName === undefined) {
      this.userName = "";
    }
    console.log("Details panel with current user: " + this.userName);

    /*
    $(document).ready(() => {
      $('#zoomImageModal').on('show.bs.modal', function (event) {
        var imgbutton = $(event.relatedTarget); // Button that triggered the modal
        var imageId = imgbutton.data('imageid'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-title').text('New message to ' + imageId)
      })
    });
    */

    this.innerWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  //
  //
  ngOnDestroy() {
  }


  /**
   *
   * @param paramName
   * @returns {boolean}
   */
  isParameterDefined(paramName) {
    for (let i = 0; i < this.document.parameters.length; i++) {
      let param = this.document.parameters[i];
      if (param.name === paramName) {
        return true;
      }
    }
    return false;
  }

  /**
   *
   * @param paramName
   * @returns {any}
   */
  getParameterValue(paramName) {
    for (let i = 0; i < this.document.parameters.length; i++) {
      let param = this.document.parameters[i];
      if (param.name === paramName) {
        return param.value;
      }
    }
    return undefined;
  }

// get a document and update the html template elements
//
  getDocument(documentId) {

    const httpOptions = {};

    // display spinner while loading data
    this.showSpinner = true;

    this.http.get(environment.backendServerUrl + '/filigrane/' + documentId, httpOptions)
      .subscribe(data => {

        // this will automaticaly update the info displayed on the web page
        this.document = <DocumentClass>data;

        // also display the image
        try {
          for (let page of this.document.pages) {
            for (let thumbnail of page.thumbnails) {
              this.images[thumbnail.imageId] = "";
              this.fetchImage(thumbnail.imageId);
            }
          }
        } catch (error) {
          // problem woth the image decoding ?
          // should display a warning
          console.log('unable to parse image');
        }

        // turn off spinner
        this.showSpinner = false;
      }, (err) => {
        // unable to fetch data. display alert about connection ?
        // turn off spinner
        this.showSpinner = false;
      });
  }

// fetch an image using its imageId and store locally
//
  fetchImage(imageId) {

    const httpOptions = {};

    this.http.get(environment.backendServerUrl + '/thumbnail/' + imageId, httpOptions)
      .subscribe(data => {
        try {
          let thumbnail = <ImageClass>data;
          this.images[thumbnail.imageId] = thumbnail.dataImg;
        } catch (error) {
          console.log('unable to parse image ' + error);
        }
      }, (err) => {
      });
  }

// Delete document based on documentId
//
  deleteDocument(documentId) {
    if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
      this.router.navigate(['login']);
    } else {
      // set the http header with the jwt token in the global storage
      const httpOptions = {
        headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
      };

      this.http.delete(environment.backendServerUrl + '/filigrane/' + documentId, httpOptions)
        .subscribe(res => {
            this.documentListService.clearList();
            this.router.navigate(['/filigranes']);
          }, (err) => {
            console.log(err);
          }
        );
    }
  }

  idUserContribution() {
    return (this.userName !== "" && (this.document.userId === this.userName));
  }


  /**
   *
   */
  cancel() {
  }

  /**
   *
   * @param documentId
   */
  deleteItem(documentId) {
    if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
      this.router.navigate(['login']);
    } else {
      if (this.document.documentId === documentId) {
        // set the http header with the jwt token in the global storage
        const httpOptions = {
          headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
        };
        this.showSpinner = true;
        this.http.delete(environment.backendServerUrl + '/filigrane/' + documentId, httpOptions)
          .subscribe(res => {
              this.documentListService.removeDocument(documentId);
              this.router.navigate(['/filigranes']);
              this.showSpinner = false;
            }, (err) => {
              this.showSpinner = false;
              console.log(err);
            }
          );
      } else {
        //
      }
    }
  }

  backSearch() {
    // go to search page
    this.router.navigate(['/filigrane-search'], {state: {isBack: true}});
  }

  routeEdit() {
    // go to search page
    this.router.navigate(['/filigrane-edit', this.document.documentId]);
  }

  routeHome() {
    // go back to main page
    this.router.navigate(['/filigranes']);
  }


  closeModal() {
    this.doShowFS = false;
    this.myFullresImage = "";
  }

  showFullscreen() {
    return !this.doShowFS;
  }

  showFullscreenImage(imageId) {

    const httpOptions = {};
    console.log("SHOW FS " + imageId);
    this.myThumbnail = this.images[imageId];

    this.http.get(environment.backendServerUrl + '/image/' + imageId, httpOptions)
      .subscribe(data => {

        // also display the image
        try {
          let image = <ImageClass>data;
          this.myFullresImage = image.dataImg;
          this.cdRef.detectChanges();

          this.compressToThumbnail();
        } catch (error) {
          console.log('unable to display full image ' + JSON.stringify(error));
        }
      }, (err) => {
      });
    this.doShowFS = true;
    this.fsImageId = imageId;
  }


  compressToThumbnail() {
    let width = this.innerWidth / 1.5;
    let height = this.innerWidth / 1.5;

    if (width < height) {
      height = width;
    } else {
      width = height;
    }

    const img = new Image();
    img.src = this.myFullresImage;
    img.onload = () => {
      const elem = document.createElement('canvas');
      elem.width = width;
      elem.height = height;
      const ctx = elem.getContext('2d');
      // img.width and img.height will contain the original dimensions
      ctx.drawImage(img, 0, 0, width, height);
      this.myThumbnail = ctx.canvas.toDataURL('image/jpeg', 1);
    }
  };
}
