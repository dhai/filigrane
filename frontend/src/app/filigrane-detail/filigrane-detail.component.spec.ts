import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FiligraneDetailComponent} from './filigrane-detail.component';

describe('FiligraneDetailComponent', () => {
  let component: FiligraneDetailComponent;
  let fixture: ComponentFixture<FiligraneDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FiligraneDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiligraneDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
