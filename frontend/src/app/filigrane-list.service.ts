import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

@Injectable()
export class FiligraneListService {

  rows: any;

  constructor() {
    this.rows = null;
  }

  addList(newList) {
    this.rows = newList;
  }

  getList(): any {
    return this.rows;
  }

  isInit(): boolean {
    if (this.rows === null || this.rows === undefined) {
      return false;
    } else {
      return true;
    }
  }

  //  clear service list. Will force list reload and table update on main page when launched
  clearList() {
    this.rows = null;
  }

  /**
   *
   */
  requestSync() {
    navigator.serviceWorker.ready.then(swRegistration => swRegistration.sync.register('todo_updated'));
  }
}
