import {Component, OnInit, Renderer2} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from "@angular/router";

import {environment} from '../../environments/environment';

import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  acceptAgreement = false;
  testAgreement = 'By clicking this box, you accept the following ' +
    '<a target="_blank" href="/assets/static/tos.html">Terms of service!</a> and ' +
    '<a target="_blank" href="/assets/static/privacy.html">Privacy policy!</a> ' +
    '/ En cliquant ici, vous acceptez les ' +
    '<a target="_blank" href="/assets/static/tos.html">Conditions d\'utilisation!</a> et la ' +
    '<a target="_blank" href="/assets/static/privacy.html">Politique de gestion des données personelles!</a>';

  signupData = {username: '', password: '', firstname: '', lastname: '', affiliation: '', email: ''};
  message = '';

  // UI language setup
  browserLangage = 'en';
  isLangInit = false;

  //
  constructor(private http: HttpClient,
              private router: Router,
              private translate: TranslateService) {
  }

  //
  ngOnInit() {
  }

  //
  //
  signup() {
    this.http.post(environment.backendServerUrl + '/user/signup', this.signupData).subscribe(resp => {
      // check the response obj properties
      if (resp['success'] === true) {
        this.router.navigate(['login']);
      } else {
        this.message = resp['msg'];
      }
    }, err => {
      this.message = err.error.msg;
    });
  }

  public onParameterChange(event, i, parameter) {
    console.log("onParameterChange " + i + ' ' + JSON.stringify(parameter));
  }

}
