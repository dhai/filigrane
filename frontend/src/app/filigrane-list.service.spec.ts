import {TestBed, inject} from '@angular/core/testing';

import {FiligraneListService} from './filigrane-list.service';

describe('FiligraneListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FiligraneListService]
    });
  });

  it('should be created', inject([FiligraneListService], (service: FiligraneListService) => {
    expect(service).toBeTruthy();
  }));
});
