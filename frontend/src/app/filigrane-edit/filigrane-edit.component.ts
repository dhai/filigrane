'use strict';

import {Component, OnInit, OnDestroy, ViewEncapsulation, ElementRef, ViewChild, Renderer2} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {DocumentListService} from '../document-list.service';

import {DocumentClass, PageClass, ThumbnailClass} from '../DocumentClass';
import {ImageClass} from '../ImageClass';

import {environment} from '../../environments/environment';

import * as uuid from 'uuid';

import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-filigrane-edit',
  templateUrl: './filigrane-edit.component.html',
  styleUrls: ['./filigrane-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FiligraneEditComponent implements OnInit, OnDestroy {
  //@ViewChild('player') player :  <HTMLVideoElement>;

  // show/hide spinner while loading data
  showSpinner = false;

  // UI language setup
  browserLangage = 'en';
  isLangInit = false;

  // filigrane data and Id
  doc: DocumentClass = new DocumentClass();

  currentId = '';

  // used to hide/show live video prior to capture - check html
  showForm = false;
  showVideo = false;
  mediaStreamTrack;

  pageIndex = 0;

  image: ImageClass = new ImageClass();
  images: String[] = [];

  // TODO check if necessary for html or not (added for AOT) - value in click for file upload
  value = null;


  //
  //
  //

  constructor(private http: HttpClient,
              private router: Router,
              private route: ActivatedRoute,
              private renderer: Renderer2,
              private documentListService: DocumentListService,
              private translate: TranslateService) {
    // cosntructor code
  }

  //
  //
  ngOnInit() {
    // get the data
    this.getDocument(this.route.snapshot.params['id']);
  }

  //
  //
  ngAfterViewInit() {
  }

  //
  //
  ngOnDestroy() {
  }

  // get filigrane info
  //
  getDocument(documentId) {
    if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
      this.router.navigate(['login']);
    } else {
      // set the http header with the jwt token in the global storage
      const httpOptions = {
        //        headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
      };

      // display spinner while loading data
      this.showSpinner = true;

      this.http.get(environment.backendServerUrl + '/filigrane/' + documentId, httpOptions)
        .subscribe(data => {

          // got info, set local
          this.doc = <DocumentClass>data;

          // async download of all images
          for (let page of this.doc.pages) {
            for (let thumbnail of page.thumbnails) {
              this.images[thumbnail.imageId] = "";
              this.fetchImage(thumbnail.imageId);
            }
          }
          // hide spinner
          this.showSpinner = false;
        }, (err) => {
          // error... network ?
          this.showSpinner = false;
          console.log(err);
        });
    }
  }

  // save changes in document
  //
  updateFiligrane(documentId) {
    this.currentId = documentId;

    if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
      this.router.navigate(['login']);
    } else {
      // set the http header with the jwt token in the global storage
      const httpOptions = {
        headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
      };

      // display spinner while uploading data
      this.showSpinner = true;

      this.http.put(environment.backendServerUrl + '/filigrane/' + this.currentId, this.doc, httpOptions)
        .subscribe(res => {
            // hide spinner
            this.showSpinner = false;

            // need to force the refresh of the list of filigranes
            this.documentListService.clearList();

            // id does not change ? let documentId = res['documentId'];
            this.router.navigate(['/filigrane-details', this.currentId], {state: {isMatchDetails: false}});
          }, (err) => {
            // error... network ?
            this.showSpinner = false;
            console.log(err);
          }
        );
    }
  }

  //
  //
  cancelUpdate() {
    this.showSpinner = false;
    this.router.navigate(['/filigrane-details', this.currentId], {state: {isMatchDetails: false}});
  }

  //
  //
  addThumbnailToPage(pageIndex) {
    this.pageIndex = pageIndex;
    this.showVideo = true;

    console.log("addThumbnailToPage startvideo");
    this.startVideo();
  }


  //
  //
  startVideo() {
    console.log("startVideo");

    const self = this;

    this.showVideo = true;
    // drag and drop t
    const supported = 'mediaDevices' in navigator;

    console.log("will wait player");

    let player = <HTMLVideoElement>document.getElementById('player');

    console.log("got player " + player);

    const capturedImage = <HTMLCanvasElement>document.getElementById('capturedImage');
    const capturedImageCtx = <CanvasRenderingContext2D>capturedImage.getContext('2d');
    const captureButton = document.getElementById('capture');

    const thumbnail = <HTMLCanvasElement>document.getElementById('thumbnailImg');
    const thumbnailCtx = <CanvasRenderingContext2D>thumbnail.getContext('2d');

    // callback to detect when video is playing. Borders should be computed then
    player.addEventListener("playing", self._isPlaying, false);

    // define the constraints for the camera vidzo stream
    // prefered back camera for phones
    // video 1280*760
    const constraints = {
      audio: false,
      video: {
        facingMode: {ideal: "environment"},
        width: 1280,
        height: 720
      },
      optional: [
        {minWidth: 320},
        {minWidth: 640},
        {minWidth: 1024},
        {minWidth: 1280},
        {minWidth: 1920},
        {minWidth: 2560},
      ]
    };

    // setup video stream
    navigator.mediaDevices.getUserMedia(constraints)
      .then((mediaStream) => {
        console.log("navigator.mediaDevices.getUserMedia");
        player.srcObject = mediaStream; // window.URL.createObjectURL(mediaStream);
        // this.mediaStreamTrack = window.URL.createObjectURL(mediaStream); // mediaStream.getVideoTracks()[0];
        //console.log ('player width and height '  + this.mediaStreamTrack.videoWidth + ' ' + this.mediaStreamTrack.videoHeight );
      });

    // setup buttons listenners

    // callback when click on capture button
    captureButton.addEventListener('click', () => {
      try {
        console.log("captureButton.addEventListener");
        // hides the video component
        self.showVideo = false;

        // compute the offset and size to take the largest crop in 2/3 portrait mode
        let height = player.videoHeight;
        let width = (height * 2) / 3;

        // if (width > player.videoWidth) {
        //  width = player.videoWidth;
        //  height = (width * 3) / 2;
        // }
        console.log('image width and height ' + width + ' ' + height);

        // assign the targeted size for the canvas holding the snapshot
        capturedImage.width = width;
        capturedImage.height = height;
        // compute the offset X and Y on the source
        const offsetX = (player.videoWidth - width) / 2;
        const offsetY = (player.videoHeight - height) / 2;

        // copy the cropped image to the target
        capturedImageCtx.drawImage(player, offsetX, offsetY, width, height, 0, 0, width, height);
        // also copy in the thumbnail
        thumbnailCtx.drawImage(player, offsetX, offsetY, width, height, 0, 0, 240, 360); // Or at whatever offset you like


        // for edit, directly upload the image
        const httpOptions = {
          headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
        };

        if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
          self.router.navigate(['login']);
        } else {
          // display spinner while loading data
          self.showSpinner = true;

          // save the cropped 2/3 image
          let format = 'image/jpeg';
          let quality = 0.75;
          self.image['dataImg'] = capturedImageCtx.canvas.toDataURL(format, quality);
          self.image['imageWidth'] = capturedImage.width;
          self.image['imageHeight'] = capturedImage.height;
          self.image['imageId'] = uuid.v4();

          // send post operation to save the filigrane info
          self.http.post(environment.backendServerUrl + '/image', self.image, httpOptions)
            .subscribe(res => {
              // save the cropped 2/3 image
              let newThumbnail: ThumbnailClass = new ThumbnailClass();
              newThumbnail['imageId'] = self.image['imageId'];
              newThumbnail['imageWidth'] = self.image['imageWidth'];
              newThumbnail['imageHeight'] = self.image['imageHeight'];

              // hides the capture/upload form component
              self.showVideo = false;
              // and show the form to add metadata to the image
              self.showForm = true;

              self.showSpinner = false;

              self.doc.pages[self.pageIndex].thumbnails.push(newThumbnail);

              // Stop all video streams.
              if (player !== null && player.srcObject !== null) {
                (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
                  track => track.stop()
                );
              }
            });

        }


      } catch (error) {
        // reject(new DOMException('UnknownError'));
        // if (videoDevice) videoDevice.stop();  // turn off the camera
      }

      // Stop all video streams
      if (player !== null && player.srcObject !== null) {
        (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
          track => track.stop()
        );
      }
    });

    // now hides the spinner and wait for user clicks
    self.showSpinner = false;

  }

  //
  //
  addPage() {
    console.log('create page, current length ' + this.doc.pages.length);

    let newPage: PageClass = new PageClass();
    newPage['thumbnails'] = <ThumbnailClass[]>[];

    this.doc.pages.push(newPage);
    console.log('addd page, new length ' + this.doc.pages.length);

    this.pageIndex = this.doc.pages.length - 1;

    this.showVideo = true;

    console.log("addPage startvideo");
    this.startVideo();
  }

  //
  //
  uploadChange(event: any) {

    // turn on the spinner while the image file is loaded/displayed
    this.showSpinner = true;

    const player = <HTMLVideoElement>document.getElementById('player');

    const capturedImage = <HTMLCanvasElement>document.getElementById('capturedImage');
    const capturedImageCtx = <CanvasRenderingContext2D>capturedImage.getContext('2d');

    const thumbnail = <HTMLCanvasElement>document.getElementById('thumbnailImg');
    const thumbnailCtx = <CanvasRenderingContext2D>thumbnail.getContext('2d');

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      // need to be accessed in callback
      const self = this;
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (readerevent) => { // called once readAsDataURL is completed
        // get image from file data
        const tmpImage = new Image;
        tmpImage.src = <string>reader.result;
        tmpImage.onload = function () {
          // when loaded, extract the center part in 2/3 format
          let height = tmpImage.height;
          let width = (height * 2) / 3;

          // assign the targeted size for the canvas holding the snapshot
          capturedImage.width = width;
          capturedImage.height = height;
          // compute the offset X and Y on the source
          const offsetX = (tmpImage.width - width) / 2;
          const offsetY = (height - height) / 2;
          // copy the cropped image to the target
          capturedImageCtx.drawImage(tmpImage, offsetX, offsetY, width, height, 0, 0, width, height);
          // also copy in the thumbnail
          thumbnailCtx.drawImage(tmpImage, offsetX, offsetY, width, height, 0, 0, 240, 360); // Or at whatever offset you like


          // for edit, directly upload the image
          const httpOptions = {
            headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
          };

          if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
            self.router.navigate(['login']);
          } else {
            // display spinner while loading data
            self.showSpinner = true;

            // save the cropped 2/3 image
            let format = 'image/jpeg';
            let quality = 0.75;
            self.image['dataImg'] = capturedImageCtx.canvas.toDataURL(format, quality);
            self.image['imageWidth'] = capturedImage.width;
            self.image['imageHeight'] = capturedImage.height;
            self.image['imageId'] = uuid.v4();

            // send post operation to save the filigrane info
            self.http.post(environment.backendServerUrl + '/image', self.image, httpOptions)
              .subscribe(res => {
                console.log('image uploaded ' + JSON.stringify(res));
                // save the cropped 2/3 image
                let newThumbnail: ThumbnailClass = new ThumbnailClass();
                newThumbnail['imageId'] = self.image['imageId'];
                newThumbnail['imageWidth'] = self.image['imageWidth'];
                newThumbnail['imageHeight'] = self.image['imageHeight'];

                // hides the capture/upload form component
                self.showVideo = false;
                // and show the form to add metadata to the image
                self.showForm = true;

                self.showSpinner = false;

                console.log('add thumbnail to page of index ' + self.pageIndex);

                self.doc.pages[self.pageIndex].thumbnails.push(newThumbnail);

                // Stop all video streams.
                if (player !== null && player.srcObject !== null) {
                  (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
                    track => track.stop()
                  );
                }
              });

          }

        };
      };
    }
    // completed the upload, turn off the spinner
    this.showSpinner = false;
  }

  //
  //
  fetchImage(imageId) {
    const httpOptions = {
      headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
    };

    this.http.get(environment.backendServerUrl + '/image/' + imageId, httpOptions)
      .subscribe(data => {

        // also display the image
        try {
          let image = <ImageClass>data;
          this.images[image.imageId] = image.dataImg;
        } catch (error) {
          console.log('unable to display thumbnail image');
        }
      }, (err) => {
      });
  }

  //
  // UI CALLBACKS
  //

  onInputChangeDocH(event: any) {
    this.doc.documentHeight = event.value;
  }

  onInputChangeDocW(event: any) {
    this.doc.documentWidth = event.value;
  }

  onInputChangeWatH(event: any) {
    this.doc.watermarkHeight = event.value;
  }

  onInputChangeWatW(event: any) {
    this.doc.watermarkWidth = event.value;
  }

  onWindowResize(event: any) {
    this._resizeVideoBorder();
  }

  //
  // PIVATE METHODS
  //


  private _resizeVideoBorder() {
    // width/height aspect ration for the capture photo
    var target_ration = 0.75
    // area for the filigrane wrt doc - 2 would be 50%, lower bigger (up to 1
    var target_area = 1.25
    var border = document.getElementById("border");

    let currentX = border.offsetWidth;
    let currentY = border.offsetHeight;
    let ratio = (currentX / currentY);

    if (ratio < target_ration) {
      currentY = Math.round((currentX / target_ration));
    } else {
      currentX = Math.round((currentY * target_ration));
    }

    let filigraneAreaX = Math.round(currentX / target_area);
    let filigraneAreaY = Math.round(currentY / target_area);

    // each border will be half of the remaining
    let borderX = Math.round((border.offsetWidth - filigraneAreaX) / 2);
    let borderY = Math.round((border.offsetHeight - filigraneAreaY) / 2);

    // use angular renderer to set style attributes
    this.renderer.setStyle(border, 'border-left-width', borderX + 'px');
    this.renderer.setStyle(border, 'border-right-width', borderX + 'px');
    this.renderer.setStyle(border, 'border-top-width', borderY + 'px');
    this.renderer.setStyle(border, 'border-bottom-width', borderY + 'px');
  }


  // Playing event
  private _isPlaying(event) {
    // indirectly will trigger a redraw of the border on top of the video the video
    window.dispatchEvent(new Event('resize'));
  };


}
