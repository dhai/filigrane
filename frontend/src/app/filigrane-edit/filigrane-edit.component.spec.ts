import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FiligraneEditComponent} from './filigrane-edit.component';

describe('FiligraneEditComponent', () => {
  let component: FiligraneEditComponent;
  let fixture: ComponentFixture<FiligraneEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FiligraneEditComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiligraneEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
