import {Injectable} from '@angular/core';
import {openDb, deleteDb} from 'idb';
import {of} from 'rxjs';
import {Observable} from 'rxjs';
import {DocumentClass} from './DocumentClass';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

import {environment} from '../environments/environment';

@Injectable()
export class DocumentListService {
  private _documentList: any;

  private isConnected: boolean = false;


  /**
   *
   */
  constructor(private router: Router,
              private http: HttpClient) {
    this._documentList = null;

    // setup DB for tmp storage of docs
    this._openDaocumentDB();


    // initialize the connection flag depending on jwt token
    if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
      this.isConnected = false;
    } else {
      this.isConnected = true;
    }

  }

  /**
   *
   * @returns {DocumentClass[]}
   */
  public get documentList(): DocumentClass[] {
    return this._documentList;
  }

  /**
   *
   * @returns {boolean}
   */
  isInit(): boolean {
    if (this._documentList === null || this._documentList === undefined) {
      return false;
    } else {
      return true;
    }
  }

  /**
   *
   * @param newList
   */
  addList(newList) {
    this._documentList = newList;
  }

  /**
   * Removes a document by its ID
   *
   * @param newList
   */
  removeDocument(documentId) {
    for (let index = 0; index < this._documentList.length; index++) {
      if (this._documentList[index].documentId === documentId) {
        this._documentList.splice(index, 1);
        index--;
      }
    }
  }

  /**
   *
   * @returns {any}
   */
  getList(): any {
    return this._documentList;
  }

  /**
   * clear service list. Will force list reload and table update on main page when launched
   *
   */
  clearList() {
    this._documentList = null;
  }

  /**
   *
   * @private
   */
  _fetchList() {

    // set the http header with the jwt token in the global storage
    const httpOptions = {
      headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
    };

    this.http.get(environment.backendServerUrl + '/filigrane', httpOptions)
      .subscribe(data => {
          this._documentList = data;
        }, err => {
          if (err.status === 401) {
            this.router.navigate(['login']);
          }
        }
      );
  }

  /**
   *
   * @private
   */
  _openDaocumentDB() {

    // opens the database for storing temporary data
    openDb('document-store', 2, upgradeDB => {
      // Note: we don't use 'break' in this switch statement,
      // the fall-through behaviour is what we want.
      switch (upgradeDB.oldVersion) {
        case 0:
          upgradeDB.createObjectStore('keyval');
        case 1:
          upgradeDB.createObjectStore('stuff', {keyPath: ''});
      }
    }).then(db => console.log("DB opened!", db));
  }

  /**
   *
   * @param buffer
   * @param type
   * @returns {Blob}
   */
  _arrayBufferToBlob(buffer, type) {
    return new Blob([buffer], {type: type});
  }

  /**
   *
   * @param blob
   * @returns {Promise<any>}
   */
  _blobToArrayBuffer(blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.addEventListener('loadend', (e) => {
        resolve(reader.result);
      });
      reader.addEventListener('error', reject);
      reader.readAsArrayBuffer(blob);
    });
  }

  //
  //
  //

  disconnect() {
    this.isConnected = false;
  }

  connect() {
    this.isConnected = true;
  }

  isUserConnected() {
    return this.isConnected;
  }

}
