import {Component} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ActivatedRoute, Router} from "@angular/router";
import {DocumentListService} from "../document-list.service";

@Component({
  selector: 'filigrane-nav',
  templateUrl: './filigrane-nav.component.html',
  styleUrls: ['./filigrane-nav.component.css']
})
export class FiligraneNavComponent {


  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private router: Router,
              private route: ActivatedRoute,
              private documentListService: DocumentListService) {
  }


  //
  //
  //


  //
  //
  stopVideoIfOn() {
    // stop all video streams.
    try {
      const player = <HTMLVideoElement>document.getElementById('player');
      if (player !== null && player.srcObject !== null) {
        (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
          track => track.stop()
        );
      }
    } catch (error) {
      // maybe player already stopped ?
    }
  }

  routeHome() {
    // stop video streams
    this.stopVideoIfOn();

    // go back to main page
    this.router.navigate(['/filigranes']);
  }

  routeSearch() {
    // stop video streams
    this.stopVideoIfOn();

    // go to search page
    this.router.navigate(['/filigrane-search'], {state: {isBack: false}});
  }

  /**
   * Logs out the current user and redirects to the login page.
   *
   */
  logout() {
    localStorage.removeItem('jwtToken');
    this.documentListService.clearList();
    this.documentListService.disconnect();
    this.router.navigate(['login']);
  }

  isConnected() {
    return this.documentListService.isUserConnected();
  }
}
