import {TestBed, inject} from '@angular/core/testing';

import {DocumentListService} from './document-list.service';

describe('DocumentListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentListService]
    });
  });

  it('should be created', inject([DocumentListService], (service: DocumentListService) => {
    expect(service).toBeTruthy();
  }));
});
