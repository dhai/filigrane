import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiligraneSearchComponent } from './filigrane-search.component';

describe('FiligraneSearchComponent', () => {
  let component: FiligraneSearchComponent;
  let fixture: ComponentFixture<FiligraneSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiligraneSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiligraneSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
