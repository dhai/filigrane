import {Component, OnInit, Renderer2} from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Router, ActivatedRoute} from '@angular/router';
import {DocumentClass, PageClass, ThumbnailClass} from '../DocumentClass';
import {ImageClass} from '../ImageClass';
import {environment} from '../../environments/environment';

import * as uuid from 'uuid';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {INSTITUTIONS} from '../fpt_constants';
import {Meta} from '@angular/platform-browser';

import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


@Component({
  selector: 'app-filigrane-search',
  templateUrl: './filigrane-search.component.html',
  styleUrls: ['./filigrane-search.component.scss']
})
export class FiligraneSearchComponent implements OnInit {

  // used to hide/show live video prior to capture - check html
  showVideo = true;
  showForm = false;
  mediaStreamTrack;

  showFormUpload = false;
  showFormCapture = false;

  // form with the matching filigranes
  showFormResults = false;

  // show/hide spinner while loading data
  showSpinner = false;

  // show corresponding thumbnail
  showThumbnail = false;

  images: string[] = [];

  myControl = new FormControl();

  value = null;

  // id of the matching watermark
  //documentMatchIds = [];
  documentMatchs = {};
  imageMatchs = {};

  documentDrawingMatchIds = [];
  //documentDrawingMatchs = {};
  //imageDrawingMatchs = {};

  documentPhotoMatchIds = [];
  //documentPhotoMatchs = {};
  //imagePhotoMatchs = {};

  //
  currentCamera = 0;
  videoselectors: any[] = [];

  isBack = false;

  isMoreResults = false;
  maxResultsPhoto = 3;
  maxResultsDrawing = 3;


  // notifications
  message = '';

  /**
   *
   * @param {HttpClient} http
   * @param {Router} router
   * @param {Meta} meta
   * @param {Renderer2} renderer
   */
  constructor(private http: HttpClient,
              private router: Router,
              private route: ActivatedRoute,
              private meta: Meta,
              private renderer: Renderer2,
              private translate: TranslateService) {

    this.meta.addTag({
      name: 'viewport',
      content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'
    });
    this.videoselectors = [];

    try {
      this.isBack = this.router.getCurrentNavigation().extras.state.isBack;
      if (this.isBack) {
        this.showFormResults = false;
        this.showVideo = false;
        this.showFormCapture = false;
        this.showFormUpload = false;

        this.showSpinner = true;

        var self = this;
        var listOfResponses = [];

        // prepare promises for fetching drawing documents
        let tmpIds = [];
        tmpIds = JSON.parse(localStorage.getItem('searchResultsDrawing'));
        for (const docId of tmpIds) {
          // geting document based on doc id.
          listOfResponses.push(this._getDocumentWithThumbnail(docId, 0));
        }

        // prepare promises for fetching photo documents
        tmpIds = [];
        tmpIds = JSON.parse(localStorage.getItem('searchResultsPhoto'));
        for (const docId of tmpIds) {
          // geting document based on doc id.
          listOfResponses.push(this._getDocumentWithThumbnail(docId, 1));
        }

        Promise.all(listOfResponses).then(function (values) {
          self.showFormResults = true;
          self.showSpinner = false;
        });

      }
    } catch (e) {
      // may not be set
      this.isBack = false;
      this.documentDrawingMatchIds = [];
      this.documentMatchs = {};
      this.imageMatchs = {};
    }
  }

  /**
   *
   */
  ngOnInit() {

  }


  /**
   * Loads the provided image data into the thumbnail elements, using image size and ration.
   * @param thumbnailImageSrc
   */
  loadThumbnailImage(thumbnailImageSrc) {
    // initialize thumbnail again with the saved image

    const capturedImage = <HTMLCanvasElement>document.getElementById('capturedImage');
    const capturedImageCtx = <CanvasRenderingContext2D>capturedImage.getContext('2d');

    const thumbnail = <HTMLCanvasElement>document.getElementById('thumbnailImg');
    const thumbnailCtx = <CanvasRenderingContext2D>thumbnail.getContext('2d');
    const thumbnailResult = <HTMLCanvasElement>document.getElementById('thumbnailImgResult');
    const thumbnailCtxResult = <CanvasRenderingContext2D>thumbnailResult.getContext('2d');

    const tmpImage = new Image;
    tmpImage.src = thumbnailImageSrc;
    tmpImage.onload = function () {
      // when loaded, extract the center part in 2/3 format
      const height = tmpImage.height;
      // const width = (height * 2) / 3;
      const width = tmpImage.width;

      // make sure image is square (ratio 1) - take smaller dimension
      let length = 0;
      let offsetX = 0;
      let offsetY = 0;
      const ratio = height / width;
      if (ratio > 1) {
        length = width;
      } else {
        length = height;
      }

      // compute the offset X and Y on the source
      offsetX = (tmpImage.width - length) / 2;
      offsetY = (tmpImage.height - length) / 2;

      // assign the targeted size for the canvas holding the snapshot
      capturedImage.width = width;
      // copy to captured image
      capturedImage.height = height;
      capturedImageCtx.drawImage(tmpImage, offsetX, offsetY, width, height, 0, 0, width, height);

      // also copy in the thumbnail
      thumbnailCtx.drawImage(tmpImage, offsetX, offsetY, length, length, 0, 0, 360, 360); // Or at whatever offset you like
      thumbnailCtxResult.drawImage(tmpImage, offsetX, offsetY, length, length, 0, 0, 180, 180); // Or at whatever offset you like
    }
  }

  /**
   *
   */
  ngAfterViewInit() {

    // if coming back from details, show previous results again
    // do not init player if coming back from search details
    // TODO check if required if doing search again
    if (this.isBack) {
      this.showVideo = false;

      // load saved thumbnail
      let thumbnailImage = JSON.parse(localStorage.getItem('searchResultsThumbnail'));
      this.loadThumbnailImage(thumbnailImage);

    } else {
      this.restartSearch();

      // now hides the spinner and wait for user clicks
      this.showSpinner = false;
    }

  }


  /**
   *
   * @param event
   */
  onWindowResize(event: any) {
    this._resizeVideoBorder();
  }

  /**
   *
   */
  showMoreLessResults() {
    if (this.maxResultsPhoto === 3) {
      this.isMoreResults = true;
      this.maxResultsPhoto = Object.keys(this.documentPhotoMatchIds).length;
      this.maxResultsDrawing = Object.keys(this.documentDrawingMatchIds).length;
    } else {
      this.isMoreResults = false;
      this.maxResultsPhoto = 3;
      this.maxResultsDrawing = 3;
    }
  }

  showMoreResults() {
    return this.isMoreResults;
  }

  // search corresponding watermarks and fetch the documents/images to display
  async getMatchingIds(photoData) {
    var self = this;

    const promise = new Promise((resolve, reject) => {
      this.http.post(environment.backendServerUrl + '/match/photo', photoData)
        .toPromise()
        .then((res: any) => {
            // Success
            const response: any = res;
            var listOfResponses = [];

            // prepare promises for fetching drawing documents
            let tmpIds = [];
            tmpIds = response.msg[0].msg.docIds;
            for (const docId of tmpIds) {
              // geting document based on doc id.
              listOfResponses.push(this._getDocumentWithThumbnail(docId, 0));
            }

            // prepare promises for fetching photo documents
            tmpIds = [];
            tmpIds = response.msg[1].msg.docIds;
            for (const docId of tmpIds) {
              // geting document based on doc id.
              listOfResponses.push(this._getDocumentWithThumbnail(docId, 1));
            }

            Promise.all(listOfResponses).then(function (values) {
              resolve();
            }, function (excpetion) {
              resolve();
            }).then(function () {
              self.showFormResults = true;
              self.showSpinner = false;

            });
          },
          err => {
            // Error
            this.showSpinner = false;
            reject(err);
          }
        );
    });
    return promise;
  }

  // get the data of the captured photo to send in the match request
  getSearchPhotoDatz() {
    // save the cropped 2/3 image
    const player = <HTMLVideoElement>document.getElementById('player');
    const capturedImage = <HTMLCanvasElement>document.getElementById('capturedImage');
    const capturedImageCtx = <CanvasRenderingContext2D>capturedImage.getContext('2d');
    const format = 'image/jpeg';

    // const quality = 0.75;
    const quality = 1.0;

    const httpOptions_ = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };

    return {'dataImg': capturedImage.toDataURL(format, quality)};
  }

  // chaining of promises to fetch both the document and the thumbnail for the first page to display
  // if both promises succeed, add the ID in the corresponding result id array depending on type (drawing or photo)
  _getDocumentWithThumbnail(documentId, matchType) {
    var self = this;

    return new Promise((resolve, reject) => {
      self._getDocumentPromise(documentId).then(function (result) {
        if (result !== null) {
          self._getThumbnailPromise(documentId).then(function (result) {

            if (matchType === 0) {
              self.documentDrawingMatchIds.push(documentId)
            } else {
              self.documentPhotoMatchIds.push(documentId)
            }
            resolve();
          })
        } else {
          resolve();
        }
      })
    })
  }

  _getThumbnailPromise(documentId) {
    var tmpDoc = this.documentMatchs[documentId];
    return new Promise((resolve, reject) => {
      try {
        for (const page of tmpDoc.pages) {
          for (const thumbnail of page.thumbnails) {
            // fetch the image asynchronously
            const httpOptions = {};
            this.http.get(environment.backendServerUrl + '/thumbnail/' + thumbnail.imageId, httpOptions)
              .subscribe(data => {
                try {
                  var thumbnail = <ImageClass>data;
                  const newMatch = {};
                  newMatch['documentId'] = documentId;
                  newMatch['thumbnail'] = thumbnail.dataImg;
                  this.imageMatchs[documentId] = newMatch;
                  resolve();
                } catch (error) {
                  // unable to parse image
                }
              }, (err) => {
                // unable to fetch image
                reject();
              });
            // then return as done hopefully
            break;
          }
          break;
        }
      } catch (error) {
        // problem woth the image decoding ? should display a warning
      }
    });
  }

  _getDocumentPromise(documentId) {

    return new Promise((resolve, reject) => {
      // set the http header with the jwt token in the global storage
      const httpOptions = {};
      this.http.get(environment.backendServerUrl + '/filigrane/' + documentId, httpOptions)
        .subscribe(async data => {
          var tmpDoc;
          if (data === null) {
            // data is null, no document at all, just return null and the ID should not be added to the display list
            resolve(null);
          } else {
            tmpDoc = <DocumentClass>data;
            // test if cast is correct (consider ok anyway as model may change)
            if (tmpDoc instanceof DocumentClass) {
            } else {
            }
            // this will automaticaly update the info displayed on the web page
            this.documentMatchs[documentId] = tmpDoc;
            resolve(documentId);
          }
        }, (error) => {
          // unable to fetch doc
          reject();
        });
    });
  }


  /**
   *
   */
  showResult() {
    // show matching filigranes
    this.showSpinner = true;
    this.showFormResults = false;

    // turn on the spinner while the model is looking for a match
    this.showThumbnail = true;

    // clear the results
    this.documentDrawingMatchIds = [];
    this.documentPhotoMatchIds = [];
    this.documentMatchs = {};
    this.imageMatchs = {};

    const photoData = this.getSearchPhotoDatz();

    this.getMatchingIds(photoData);

  }

  refresh(): void {
    window.location.reload();
  }

  /**
   *
   * @param event
   */
  uploadChange(event: any) {

    // turn on the spinner while the image file is loaded/displayed
    this.showSpinner = true;

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      // need to be accessed in callback
      const self = this;
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (readerevent) => { // called once readAsDataURL is completed
        // get image from file data
        const tmpImage = new Image;
        tmpImage.src = <string>reader.result;

        localStorage.setItem('searchResultsThumbnail', JSON.stringify(<string>reader.result));
        self.loadThumbnailImage(<string>reader.result);

        // hides the capture/upload form component
        self.showVideo = false;
        // and show the form to add metadata to the image
        self.showForm = true;
        self.showFormUpload = false;
        self.showFormCapture = true;

        // Stop all video streams.
        const player = <HTMLVideoElement>document.getElementById('player');
        if (player !== null && player.srcObject !== null) {
          (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
            track => track.stop()
          );
        }
      };
    }

    // completed the upload, turn off the spinner
    this.showSpinner = false;
  }

  /**
   * Redirects the app to the details page
   *
   * @param documentId
   */
  detailsItem(documentId: any) {
    localStorage.setItem('searchResultsDrawing', JSON.stringify(this.documentDrawingMatchIds));
    localStorage.setItem('searchResultsPhoto', JSON.stringify(this.documentPhotoMatchIds));
    this.router.navigate(['/filigrane-details', documentId], {state: {isMatchDetails: true}});
  }

  /**
   *
   */
  switchCamera() {
    const player = <HTMLVideoElement>document.getElementById('player');

    // Stop all video streams
    if (player !== null && player.srcObject !== null) {
      (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
        track => track.stop()
      );
    }

    this.currentCamera = ((this.currentCamera + 1) % this.videoselectors.length);
    const constraints = {
      video: {deviceId: this.videoselectors[this.currentCamera].value ? {exact: this.videoselectors[this.currentCamera].value} : undefined}
    };
    navigator.mediaDevices.getUserMedia(constraints).then(
      (stream) => {
        // window.stream = stream; // make stream available to console
        const player = <HTMLVideoElement>document.getElementById('player');
        player.srcObject = stream;
        // this.mediaStreamTrack = window.URL.createObjectURL(stream); // mediaStream.getVideoTracks()[0];
        this._resizeVideoBorder();
      }).catch(this._handleError);
  }

  //
  // PRIVATE METHODS
  //

  /**
   *
   * @private
   */
  private _resizeVideoBorder() {
    // width/height aspect ration for the capture photo
    const target_ration = 0.75;
    // area for the filigrane wrt doc - 2 would be 50%, lower bigger (up to 1
    const target_area = 1.25;
    const border = document.getElementById('border');

    let currentX = border.offsetWidth;
    let currentY = border.offsetHeight;
    const ratio = (currentX / currentY);

    if (ratio < target_ration) {
      currentY = Math.round((currentX / target_ration));
    } else {
      currentX = Math.round((currentY * target_ration));
    }

    const filigraneAreaX = Math.round(currentX / target_area);
    const filigraneAreaY = Math.round(currentY / target_area);

    // each border will be half of the remaining
    const borderX = Math.round((border.offsetWidth - filigraneAreaX) / 2);
    const borderY = Math.round((border.offsetHeight - filigraneAreaY) / 2);

    // use angular renderer to set style attributes
    this.renderer.setStyle(border, 'border-left-width', borderX + 'px');
    this.renderer.setStyle(border, 'border-right-width', borderX + 'px');
    this.renderer.setStyle(border, 'border-top-width', borderY + 'px');
    this.renderer.setStyle(border, 'border-bottom-width', borderY + 'px');
  }


  /**
   *
   * @param error
   * @private
   */
  private _handleError(error) {
    console.log('navigator.getUserMedia error: ', error);
  }

  // Playing event
  private _isPlaying(event) {
    // indirectly will trigger a redraw of the border on top of the video the video
    window.dispatchEvent(new Event('resize'));
  }

  /**
   *
   * @param deviceInfos
   * @private
   */
  private _gotDevices(deviceInfos) {
    this.videoselectors = [];
    // Handles being called several times to update labels. Preserve values.
    for (let i = 0; i !== deviceInfos.length; ++i) {
      const deviceInfo = deviceInfos[i];
      const option = {};
      option['value'] = deviceInfo.deviceId;
      if (deviceInfo.kind === 'videoinput') {
        option['text'] = deviceInfo.label; // || `camera ${videoselectors.length + 1}`;
        this.videoselectors.push(option);
      } else {
        console.log('Some other kind of source/device: ', deviceInfo);
      }
    }
  }


  /**
   *
   */
  async restartSearch() {

    this.showVideo = true;
    this.showForm = false;
    this.mediaStreamTrack;
    this.showFormUpload = false;
    this.showFormCapture = false;
    this.showFormResults = false;
    this.showThumbnail = false;

    this.images = [];

    // id of the matching watermark
    //this.documentMatchIds = [];
    this.documentMatchs = {};
    this.imageMatchs = {};

    this.documentDrawingMatchIds = [];
    //this.documentDrawingMatchs = {};
    //this.imageDrawingMatchs = {};

    this.documentPhotoMatchIds = [];
    //this.documentPhotoMatchs = {};
    //this.imagePhotoMatchs = {};


    this.currentCamera = 0;
    this.videoselectors = [];

    this.isBack = false;

    this.isMoreResults = false;

    this.maxResultsPhoto = 3;
    this.maxResultsDrawing = 3;

    const supported = 'mediaDevices' in navigator;
    const self = this;

    // camera preview
    // const contentVideoElement = document.getElementById('content');
    navigator.mediaDevices.enumerateDevices().then((deviceInfos) => {
      self._gotDevices(deviceInfos);
      // self.videoselectors = [];
      //
      // // Handles being called several times to update labels. Preserve values.
      // for (let i = 0; i !== deviceInfos.length; ++i) {
      //   const deviceInfo = deviceInfos[i];
      //   const option = {};
      //   option['value'] = deviceInfo.deviceId;
      //   if (deviceInfo.kind === 'videoinput') {
      //     option['text'] = deviceInfo.label || `camera ${self.videoselectors.length + 1}`;
      //     self.videoselectors.push(option);
      //   } else {
      //     console.log('Some other kind of source/device: ', deviceInfo);
      //   }
      // }
    }).catch(self._handleError);

    // wait for player element to appear (ngif)
    while (!document.getElementById("player")) {
      await new Promise(r => setTimeout(r, 200));
    }

    const player = <HTMLVideoElement>document.getElementById('player');
    const capturedImage = <HTMLCanvasElement>document.getElementById('capturedImage');
    const capturedImageCtx = <CanvasRenderingContext2D>capturedImage.getContext('2d');
    const captureButton = document.getElementById('capture');

    // callback to detect when video is playing. Borders should be computed then
    player.addEventListener('playing', self._isPlaying, false);

    // define the constraints for the camera vidzo stream
    // prefered back camera for phones
    // video 1280*760
    const constraints = {
      audio: false,
      video: {
        facingMode: {ideal: 'environment'},
        width: 1280,
        height: 720
      },
      optional: [
        {minWidth: 320},
        {minWidth: 640},
        {minWidth: 1024},
        {minWidth: 1280},
        {minWidth: 1920},
        {minWidth: 2560},
      ]
    };

    // setup video stream
    navigator.mediaDevices.getUserMedia(constraints)
      .then((mediaStream) => {
        player.srcObject = mediaStream; // window.URL.createObjectURL(mediaStream);
        // this.mediaStreamTrack = window.URL.createObjectURL(mediaStream); // mediaStream.getVideoTracks()[0];
        this._resizeVideoBorder();
      });

    // setup buttons listenners

    // callback when click on capture button
    captureButton.addEventListener('click', () => {
      try {
        // hides the video component
        this.showVideo = false;
        // and show the form to add metadata to the image
        this.showForm = true;
        this.showFormUpload = false;
        this.showFormCapture = true;

        // compute the offset and size to take the largest crop in 2/3 portrait mode
        let height = player.videoHeight;
        let width = player.videoWidth;
        // const width = (height * 2) / 3;
        const ratio = height / width;
        if (ratio > 1) {
          height = width;
        } else {
          width = height;
        }

        // assign the targeted size for the canvas holding the snapshot
        capturedImage.width = width;
        capturedImage.height = height;
        // compute the offset X and Y on the source
        const offsetX = (player.videoWidth - width) / 2;
        const offsetY = (player.videoHeight - height) / 2;

        // copy the cropped image to the target
        capturedImageCtx.drawImage(player, offsetX, offsetY, width, height, 0, 0, width, height);

        // also save capture image to support back from details
        const format = 'image/jpeg';
        const quality = 1.0;
        localStorage.setItem('searchResultsThumbnail', JSON.stringify(capturedImage.toDataURL(format, quality)));
        this.loadThumbnailImage(capturedImage.toDataURL(format, quality));
      } catch (error) {
        // reject(new DOMException('UnknownError'));
      }

      // Stop all video streams
      if (player !== null && player.srcObject !== null) {
        (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
          track => track.stop()
        );
      }
    });
  }
}
