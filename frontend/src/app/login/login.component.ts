import {Component, OnInit, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

import {environment} from '../../environments/environment';

import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {DocumentListService} from "../document-list.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, OnDestroy {

  loginData = {username: '', password: ''};
  message = '';
  data: any;


  // UI language setup
  browserLangage = 'en';
  isLangInit = false;

  displaySignUp = environment.enable_registration;

  constructor(private http: HttpClient,
              private router: Router,
              private documentListService: DocumentListService,
              private translate: TranslateService) {
  }

  ngOnInit() {
  }


  ngOnDestroy() {
  }

  //
  login() {
    this.http.post(environment.backendServerUrl + '/user/signin', this.loginData).subscribe(resp => {
      this.data = resp;

      console.log('got the jwt token ' + this.data.token);

      this.documentListService.connect();

      // save the token to the global storage object
      localStorage.setItem('jwtToken', this.data.token);
      localStorage.setItem('fptLogin', this.loginData.username);
      //console.log('generated token ' + this.data.token );
      this.router.navigate(['filigranes']);
    }, err => {
      this.message = err.error.msg;
    });
  }

}
