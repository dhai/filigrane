import {NgModule} from '@angular/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import {AppComponent} from './app.component';
import {FiligraneComponent} from './filigrane/filigrane.component';

import {RouterModule, Routes} from '@angular/router';

import {FiligraneDetailComponent} from './filigrane-detail/filigrane-detail.component';
import {FiligraneCreateComponent} from './filigrane-create/filigrane-create.component';
import {FiligraneEditComponent} from './filigrane-edit/filigrane-edit.component';
import {FiligraneSearchComponent} from './filigrane-search/filigrane-search.component';
import {FiligraneNavComponent} from './filigrane-nav/filigrane-nav.component';

import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';

import {FooterComponent} from './footer.component';
import {HeaderComponent} from './header.component';

import {ConfirmDialogDirective} from './ConfirmDialog';

import {NgcCookieConsentModule, NgcCookieConsentConfig} from 'ngx-cookieconsent';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// import Material modules - add in specific ngmaterial module below
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import {MatIconModule} from "@angular/material/icon";
import {
  GestureConfig, MatButtonModule, MatSliderModule, MatCheckboxModule, MatRadioModule, MatSelectModule,
  MatFormFieldModule, MatAutocompleteModule, MatIconModule
} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatInputModule, MatToolbarModule, MatSidenavModule, MatListModule} from '@angular/material';
import {BrowserModule, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';

import { NgxImageZoomModule } from 'ngx-image-zoom';
import {PinchZoomModule} from 'ngx-pinch-zoom';

// datatable
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {FiligraneListService} from './filigrane-list.service';
import {DocumentListService} from './document-list.service';
import {ServiceWorkerModule} from '@angular/service-worker';
import {ConnectionService} from './connection.service';
import {environment} from '../environments/environment';

// toast
import {ToastrModule} from 'ngx-toastr';

import {LayoutModule} from '@angular/cdk/layout';

import {CommonModule} from "@angular/common";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const appRoutes: Routes = [
  {
    path: 'filigranes',
    component: FiligraneComponent,
    data: {title: 'Filigrane List'}
  },
  {
    path: 'filigrane-details/:id',
    component: FiligraneDetailComponent,
    data: {title: 'Filigrane Details'}
  },
  {
    path: 'filigrane-create',
    component: FiligraneCreateComponent,
    data: {title: 'Create Filigrane'}
  },
  {
    path: 'filigrane-edit/:id',
    component: FiligraneEditComponent,
    data: {title: 'Edit Filigrane'}
  },
  {
    path: 'filigrane-search',
    component: FiligraneSearchComponent,
    data: {title: 'Search Filigrane'}
  },
  {
    path: 'filigrane-nav',
    component: FiligraneNavComponent,
    data: {title: 'Menu'}
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {title: 'Login'}
  },
  {
    path: 'signup',
    component: SignupComponent,
    data: {title: 'Sign Up'}
  },
  {
    path: '',
    redirectTo: '/filigrane-search',
    pathMatch: 'full'
  }
];

const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: 'filigranes.paris.inria.fr' // or 'your.domain.com' // it is mandatory to set a domain, for cookies to work properly (see https://goo.gl/S2Hy2A)
  },
  palette: {
    popup: {
      background: '#000'
    },
    button: {
      background: '#f1d600'
    }
  },
  theme: 'edgeless',
  type: 'opt-out',
  layout: 'my-custom-layout',
  layouts: {
    "my-custom-layout": '{{messagelink}}{{compliance}}'
  },
  elements: {
    messagelink: `
    <span id="cookieconsent:desc" class="cc-message">{{message}} 
      <a aria-label="En savoir plus sur les conditions d'utilisation du service" tabindex="2" class="cc-link" href="{{tosHref}}" target="_blank">{{tosLink}}</a>
      <a aria-label="En savoir plus sur la Politique de gestion des données personelles" tabindex="1" class="cc-link" href="{{privacyPolicyHref}}" target="_blank">{{privacyPolicyLink}}</a> 
    </span>
    `,
  },
  content: {
    allow: 'Accepter',
    deny: 'Refuser',

    message: 'En poursuivant votre navigation sur ce site, vous acceptez les conditions d\'utilisations du service et l\’utilisation de Cookies pour réaliser des statistiques de visites',

    tosLink: 'Conditions d\'utilisation du service',
    tosHref: 'https://filigranes.paris.inria.fr/assets/static/tos.html',

    privacyPolicyLink: 'Politique de gestion des données personelles',
    privacyPolicyHref: 'https://filigranes.paris.inria.fr/assets/static/privacy.html',
  }
};

@NgModule({
  exports: [
    NgbModule,
    MatButtonModule,
    MatToolbarModule,
    MatSliderModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    BrowserAnimationsModule
  ],
  providers: [FiligraneListService, DocumentListService, ConnectionService],
  imports: [LayoutModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule],
  declarations: []
})
export class AllMaterialModule {
}


@NgModule({
  declarations: [
    AppComponent,
    FiligraneComponent,
    FiligraneDetailComponent,
    FiligraneCreateComponent,
    FiligraneEditComponent,
    FiligraneSearchComponent,
    FiligraneNavComponent,
    LoginComponent,
    SignupComponent,
    FooterComponent,
    HeaderComponent,
    ConfirmDialogDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AllMaterialModule,
    NgxDatatableModule,
    MatSidenavModule,
    MatListModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      {useHash: true, enableTracing: false} // <-- enableTracing debugging purposes only
    ),
    PinchZoomModule,
    NgxImageZoomModule.forRoot(
    ),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgcCookieConsentModule.forRoot(
      cookieConfig
    ),
    ServiceWorkerModule.register('/sw-master.js', {enabled: environment.production})
    // ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production})
  ],
  providers: [
    {provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
