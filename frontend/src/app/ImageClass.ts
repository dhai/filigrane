'use strict';

export class ImageClass {

  constructor() {
  };

  _id: string;
  imageId: string;
  dataImg: string;
  imageWidth: number;
  imageHeight: number;
}
