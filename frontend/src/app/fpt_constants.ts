export const INSTITUTIONS = ['Archives Nationales - Minutier central', 'Ecole Nationale des Chartes', 'Archives municipales de Toulouse', 'Archives municipales de Bordeaux'];
export const DOCTYPES = ['écrit public/professionnel', 'écrit privé', 'dessin (y compris plan)', 'texte imprimé', 'estampe'];
