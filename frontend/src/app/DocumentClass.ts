'use strict';

export class ThumbnailClass {

  constructor() {
  };

  imageId: string;
  dataImg: string;
  imageWidth: number;
  imageHeight: number;
}

export class ParameterClass {
  category: string;
  value: string;
  name: string;

  constructor() {
  };
}

export class PageClass {

  pageId: string;
  pageNumber: number;

  writingOrigin: string;
  writingDate: string;

  documentWidth: number;
  documentHeight: number;
  watermarkWidth: number;
  watermarkHeight: number;

  thumbnails: ThumbnailClass[];
  parameters: ParameterClass[];


  constructor() {
    this.thumbnails = [];
    this.parameters = [];
  };

}

export class DocumentClass {

  _id: string;

  userId: string;
  documentId: string;

  institution: string;
  reference: string;

  documentType: string;
  documentWidth: number;
  documentHeight: number;
  watermarkWidth: number;
  watermarkHeight: number;
  watermarkShape: string;


  totalPages: number;

  description: string;
  keywords: string;

  TPQ: number;
  TAQ: number;

  allowPhotoRepro: boolean;
  allowPhotoTrain: boolean;

  pages: PageClass[];
  parameters: ParameterClass[];

  updatedDate: Date;


  constructor() {
    this.parameters = [];
    this.pages = [];
  };


}

