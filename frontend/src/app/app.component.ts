import {Component, OnInit} from '@angular/core';
import {SwUpdate} from '@angular/service-worker';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {ToastrService} from 'ngx-toastr';
import {ConnectionService} from './connection.service';

import {environment} from '../environments/environment';

import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {NgcCookieConsentService, NgcStatusChangeEvent} from 'ngx-cookieconsent';
import {Observable} from 'rxjs';
import {Subscription} from 'rxjs/Subscription';
import {DocumentListService} from "./document-list.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'app';
  isConnected = true;

  // UI language setup
  browserLangage = 'en';
  isLangInit = false;

  private statusChangeSubscription: Subscription;

  constructor(private router: Router,
              private http: HttpClient,
              private swUpdate: SwUpdate,
              private toastr: ToastrService,
              private connectionService: ConnectionService,
              private ccService: NgcCookieConsentService,
              private translate: TranslateService) {

    // UI language setup
    try {
      this.browserLangage = window.navigator['userLanguage'] || window.navigator.language;
      if (this.browserLangage !== 'fr' && this.browserLangage !== 'fr-FR') {
        this.browserLangage = 'en';
      } else {
        this.browserLangage = 'fr'
      }
    } catch (err) {
    }

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang(this.browserLangage);
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use(this.browserLangage);

    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.showOnline();
      }
      else {
        this.showOffline();
      }
    });

    // Install service worker for offline use and caching
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register(environment.serviceWorkerScript, {scope: '/'});

      // reacts to messages from the service worker
      navigator.serviceWorker.addEventListener('message', event => {
        if (event.data.category === 'toast') {
          this.showWarning(event.data.title, event.data.msg);
        }
      });
    }
  }

  ngOnInit() {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('New version available. Load New Version?')) {
          window.location.reload();
        }
      });
    }

    let self = this;
    this.statusChangeSubscription = this.ccService.statusChange$.subscribe(
      (event: NgcStatusChangeEvent) => {
        try {
            if (event.status === "deny") {
              window.location.href = "https://filigranes.hypotheses.org/le-projet";
            } else  if (event.status === "allow") {
              this.statusChangeSubscription.unsubscribe();
              this.ccService.destroy();//remove previous cookie bar (with default messages)
            } else {
            }
        } catch (e) {

        }
      });



    this.translate//
      .get(['cookie.header', 'cookie.message', 'cookie.dismiss', 'cookie.allow', 'cookie.deny', 'cookie.link', 'cookie.policy'])
      .subscribe(data => {

        this.ccService.getConfig().content = this.ccService.getConfig().content || {} ;
        // Override default messages with the translated ones
        this.ccService.getConfig().content.header = data['cookie.header'];
        this.ccService.getConfig().content.message = data['cookie.message'];
        this.ccService.getConfig().content.dismiss = data['cookie.dismiss'];
        this.ccService.getConfig().content.allow = data['cookie.allow'];
        this.ccService.getConfig().content.deny = data['cookie.deny'];
        this.ccService.getConfig().content.link = data['cookie.link'];
        this.ccService.getConfig().content.policy = data['cookie.policy'];

        this.ccService.destroy();//remove previous cookie bar (with default messages)
        this.ccService.init(this.ccService.getConfig()); // update config with translated messages
      });

  }

  //
  ngOnDestroy() {
      this.statusChangeSubscription.unsubscribe();
  }

  //
  // notification display operation
  //

  private showOffline() {
    this.toastr.error('Client is offline', 'Error');
  }

  private showOnline() {
    this.toastr.success('Client is online', 'Success');
  }

  private showWarning(title, msg) {
    this.toastr.warning(msg, title);
  }



}
