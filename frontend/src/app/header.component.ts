import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DocumentListService} from "./document-list.service";

@Component({
  selector: 'app-header',
  template: `
  <div class="header">
    <div class="container">
      <div class="row">
        <div class="col-md-8" style="width:auto;display: flex;align-items: center;">
          <h2><a style= "font-size:6vw;color: white;" target="_blank" href="https://filigranes.hypotheses.org/le-projet">Filigranes Pour Tous</a></h2>
        </div>
        <div class="col-md-4" style="display: flex;flex-direction:row;width:auto;">
          <div style="display: flex;">
            <button  type="button" class="btn btn-secondary" (click)="routeSearch()" style="flex;align-items: center;">
              <mat-icon >image_search</mat-icon>
              <span > {{ 'html.identify' | translate }}</span>
            </button>
          </div>     
          <div style="display: flex;">
            <button type="button" class="btn btn-secondary" (click)="routeHome()" style="flex;align-items: center;">
              <mat-icon >home</mat-icon>
              <span > {{ 'html.inventory' | translate }}</span>
            </button>
          </div>
          <div [hidden]="!isConnected()" style="display: flex;">
            <button type="button" class="btn btn-danger" (click)="logout()" style="vertical-align: middle;">
              <mat-icon style="vertical-align: middle;">power_settings_new</mat-icon>
              <span> {{ 'html.logout' | translate }}</span>
             </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  `,
  styleUrls: ['./app.component.css']
})
export class HeaderComponent implements OnInit {

  isToTo = false;

  constructor(              private router: Router,
                            private route: ActivatedRoute,
                            private documentListService: DocumentListService) {
  }

  ngOnInit() {
  }

  //
  //
  stopVideoIfOn() {
    // stop all video streams.
    try {
      const player = <HTMLVideoElement>document.getElementById('player');
      if (player !== null && player.srcObject !== null) {
        (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
          track => track.stop()
        );
      }
    } catch (error) {
      // maybe player already stopped ?
    }
  }

  routeHome() {
    // stop video streams
    this.stopVideoIfOn();

    // go back to main page
    this.router.navigate(['/filigranes']);
  }

  routeSearch() {
    // stop video streams
    this.stopVideoIfOn();

    // go to search page
    this.router.navigate(['/filigrane-search'], {state: {isBack: false}});
  }

  /**
   * Logs out the current user and redirects to the login page.
   *
   */
  logout() {
    localStorage.removeItem('jwtToken');
    this.documentListService.clearList();
    this.documentListService.disconnect();
    this.router.navigate(['login']);
  }

  isConnected() {
    return this.documentListService.isUserConnected();
  }

}
