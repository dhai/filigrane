import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <div class="footer_row">
      <div class="footer_item">
        <img src="/assets/images/logo_enpc.png" />
      </div>
      <div class="footer_item">
        <img src="/assets/images/logo_an.jpg" />
      </div>
      <div class="footer_item">
        <img src="/assets/images/logo_psl.jpg" />
      </div>
      <div class="footer_item">
        <img src="/assets/images/logo_irht.jpg" />
      </div>
      <div class="footer_item">
        <img src="/assets/images/logo_inria.jpg" />
      </div>
      <div class="footer_item">
        <img src="/assets/images/logo_enc2.png" />
      </div>
      <div class="footer_item">
        <img src="/assets/images/logo_enc_cjm.png" />
      </div>
    </div>
  `,
  styleUrls: ['./app.component.css']
})

export class FooterComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
