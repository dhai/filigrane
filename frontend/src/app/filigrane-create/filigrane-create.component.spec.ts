import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FiligraneCreateComponent} from './filigrane-create.component';

describe('FiligraneCreateComponent', () => {
  let component: FiligraneCreateComponent;
  let fixture: ComponentFixture<FiligraneCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FiligraneCreateComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiligraneCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
