import {Component, OnInit, ViewEncapsulation, Renderer2} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Meta} from '@angular/platform-browser';
import {DocumentListService} from '../document-list.service';

import {DocumentClass, PageClass, ThumbnailClass} from '../DocumentClass';
import {ImageClass} from '../ImageClass';

import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {map, startWith} from 'rxjs/operators';

import {INSTITUTIONS} from "../fpt_constants";
import {DOCTYPES} from "../fpt_constants";

import {environment} from '../../environments/environment';

import * as uuid from 'uuid';

import {LangChangeEvent, TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-filigrane-create',
  templateUrl: './filigrane-create.component.html',
  styleUrls: ['./filigrane-create.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class FiligraneCreateComponent implements OnInit {

  //
  currentCamera = 0;
  videoselectors: any[] = [];
  // mediaStreamTrack;

  // used to hide/show live video prior to capture - check html
  showVideo = true;
  showForm = false;

  showFormUpload = false;
  showFormCapture = false;

  // show/hide spinner while loading data
  showSpinner = false;

  // UI language setup
  browserLangage = 'en';
  isLangInit = false;

  // references to the video/stream/image components
  document = new DocumentClass();
  page = new PageClass();
  thumbnail = new ThumbnailClass();
  image = new ImageClass();

  //
  myControl = new FormControl();

  //
  institutions: string[] = INSTITUTIONS;
  docTypes: string[] = DOCTYPES;

  filteredInstitutions: Observable<string[]>;
  advancedInfo = false;

  // TODO check if necessary for html or not (added for AOT) - value in click for file upload
  value = null;

  //

  //
  //
  //

  //
  constructor(private router: Router,
              private http: HttpClient,
              private meta: Meta,
              private renderer: Renderer2,
              private documentListService: DocumentListService,
              private translate: TranslateService) {
    this.meta.addTag({
      name: 'viewport',
      content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'
    });
    this.videoselectors = [];
  }

  //
  ngOnInit() {

    // initialize some of the document value - do before view init
    this.document['allowPhotoRepro'] = true;
    this.document['allowPhotoTrain'] = true;
    this.document['pages'] = <PageClass[]>[];

    this.page['documentWidth'] = 0;
    this.page['documentHeight'] = 0;
    this.page['watermarkWidth'] = 0;
    this.page['watermarkHeight'] = 0;
    this.page['pageNumber'] = 0;
    this.page['thumbnails'] = <ThumbnailClass[]>[];

    this.thumbnail['imageWidth'] = 0;
    this.thumbnail['imageHeight'] = 0;

    this.image['imageWidth'] = 0;
    this.image['imageHeight'] = 0;

    this.filteredInstitutions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

  }


  //
  ngAfterViewInit() {

    // drag and drop t
    const supported = 'mediaDevices' in navigator;
    const self = this;

    if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
      this.router.navigate(['login']);
    } else {

      // display spinner while loading data
      this.showSpinner = true;

      // camera preview
      // const contentVideoElement = document.getElementById('content');
      navigator.mediaDevices.enumerateDevices().then((deviceInfos) => {
        self._gotDevices(deviceInfos);
        // self.videoselectors = [];
        //
        // // Handles being called several times to update labels. Preserve values.
        // for (let i = 0; i !== deviceInfos.length; ++i) {
        //   const deviceInfo = deviceInfos[i];
        //   const option = {};
        //   option['value'] = deviceInfo.deviceId;
        //   if (deviceInfo.kind === 'videoinput') {
        //     option['text'] = deviceInfo.label || `camera ${self.videoselectors.length + 1}`;
        //     self.videoselectors.push(option);
        //   } else {
        //     console.log('Some other kind of source/device: ', deviceInfo);
        //   }
        // }
      }).catch(self._handleError);

      const player = <HTMLVideoElement>document.getElementById('player');
      const capturedImage = <HTMLCanvasElement>document.getElementById('capturedImage');
      const capturedImageCtx = <CanvasRenderingContext2D>capturedImage.getContext('2d');
      const captureButton = document.getElementById('capture');

      const thumbnail = <HTMLCanvasElement>document.getElementById('thumbnailImg');
      const thumbnailCtx = <CanvasRenderingContext2D>thumbnail.getContext('2d');

      // callback to detect when video is playing. Borders should be computed then
      player.addEventListener("playing", self._isPlaying, false);

      // define the constraints for the camera vidzo stream
      // prefered back camera for phones
      // video 1280*760
      const constraints = {
        audio: false,
        video: {
          facingMode: {ideal: "environment"},
          width: 1280,
          height: 720
        },
        optional: [
          {minWidth: 320},
          {minWidth: 640},
          {minWidth: 1024},
          {minWidth: 1280},
          {minWidth: 1920},
          {minWidth: 2560},
        ]
      };

      // setup video stream
      navigator.mediaDevices.getUserMedia(constraints)
        .then((mediaStream) => {
          player.srcObject = mediaStream; // window.URL.createObjectURL(mediaStream);
          // this.mediaStreamTrack = window.URL.createObjectURL(mediaStream); // mediaStream.getVideoTracks()[0];
          this._resizeVideoBorder();
        });

      // setup buttons listenners

      // callback when click on capture button
      captureButton.addEventListener('click', () => {
        try {
          // hides the video component
          this.showVideo = false;
          // and show the form to add metadata to the image
          this.showForm = true;
          this.showFormUpload = false;
          this.showFormCapture = true;

          // compute the offset and size to take the largest crop in 2/3 portrait mode
          let height = player.videoHeight;
          let width = (height * 2) / 3;

          // if (width > player.videoWidth) {
          //  width = player.videoWidth;
          //  height = (width * 3) / 2;
          // }

          //console.log ('image width and height '  + width + ' ' + height );

          // assign the targeted size for the canvas holding the snapshot
          capturedImage.width = width;
          capturedImage.height = height;
          // compute the offset X and Y on the source
          const offsetX = (player.videoWidth - width) / 2;
          const offsetY = (player.videoHeight - height) / 2;

          // copy the cropped image to the target
          capturedImageCtx.drawImage(player, offsetX, offsetY, width, height, 0, 0, width, height);
          // also copy in the thumbnail
          thumbnailCtx.drawImage(player, offsetX, offsetY, width, height, 0, 0, 240, 360); // Or at whatever offset you like

        } catch (error) {
          // reject(new DOMException('UnknownError'));
          // if (videoDevice) videoDevice.stop();  // turn off the camera
        }

        // Draw the video frame to the canvas and prepare the object to be posted to the server
        let format = 'image/jpeg';
        let quality = 0.75;
        this.image['dataImg'] = capturedImageCtx.canvas.toDataURL(format, quality);
        this.image['imageWidth'] = capturedImage.width;
        this.image['imageHeight'] = capturedImage.height;
        this.image['imageId'] = uuid.v4();

        // Stop all video streams
        if (player !== null && player.srcObject !== null) {
          (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
            track => track.stop()
          );
        }
      });

      // now hides the spinner and wait for user clicks
      this.showSpinner = false;
    }
  }


  //
  //
  routeHome() {
    // turn off the spinner
    this.showSpinner = false;

    // stop all video streams.
    try {
      const player = <HTMLVideoElement>document.getElementById('player');
      if (player !== null && player.srcObject !== null) {
        (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
          track => track.stop()
        );
      }
    } catch (error) {
      // maybe player already stopped ?
    }

    // go back to main page
    this.router.navigate(['/filigranes']);
  }

  routeSearch() {
    // go to search page
    this.router.navigate(['/filigrane-search'], {state: {isBack: false}});
  }

  // send the filignrane photo and info to the server
  // and switch to the details view component
  //
  saveFiligrane() {
    // set the http header with the jwt token in the global storage
    const httpOptions = {
      headers: new HttpHeaders({'Authorization': localStorage.getItem('jwtToken')})
    };

    if (localStorage.getItem('jwtToken') === null || localStorage.getItem('jwtToken') === undefined) {
      this.router.navigate(['login']);
    } else {

      // display spinner while loading data
      this.showSpinner = true;

      // set the height and with of the captured image
      // const capturedImage = <HTMLCanvasElement>document.getElementById('capturedImage');

      // send post operation to save the filigrane info
      this.http.post(environment.backendServerUrl + '/image', this.image, httpOptions)
        .subscribe(res => {
            // update thumbnail info
            this.thumbnail['imageId'] = this.image['imageId'];
            this.thumbnail['imageWidth'] = this.image['imageWidth'];
            this.thumbnail['imageHeight'] = this.image['imageHeight'];
            // add thumbnail as page info
            this.page['thumbnails'].push(this.thumbnail);
            this.page['pageNumber'] = 1;
            // add page to document
            this.document['pages'] = [];
            this.document['pages'].push(this.page);
            this.document['totalPages'] = 1;
            // generate uuid for document
            this.document['documentId'] = uuid.v4();

            // send post operation to save the filigrane info
            this.http.post(environment.backendServerUrl + '/filigrane', this.document, httpOptions)
              .subscribe(res => {
                  this.documentListService.clearList();
                  this.showSpinner = false;
                  // let documentId = res['documentId'];
                  this.router.navigate(['/filigrane-details', this.document.documentId], {state: {isMatchDetails: false}});
                }, (err) => {
                  this.showSpinner = false;
                  console.log(err);
                }
              );
          }, (err) => {
            this.showSpinner = false;
            console.log(err);
          }
        );
    }
  }

  /**
   *
   * @param event
   */
  uploadChange(event: any) {

    // turn on the spinner while the image file is loaded/displayed
    this.showSpinner = true;

    const player = <HTMLVideoElement>document.getElementById('player');
    const capturedImage = <HTMLCanvasElement>document.getElementById('capturedImage');
    const capturedImageCtx = <CanvasRenderingContext2D>capturedImage.getContext('2d');
    const thumbnail = <HTMLCanvasElement>document.getElementById('thumbnailImg');
    const thumbnailCtx = <CanvasRenderingContext2D>thumbnail.getContext('2d');

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      // need to be accessed in callback
      const self = this;
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (readerevent) => { // called once readAsDataURL is completed
        // get image from file data
        const tmpImage = new Image;
        tmpImage.src = <string>reader.result;
        tmpImage.onload = function () {
          // when loaded, extract the center part in 2/3 format
          let height = tmpImage.height;
          let width = (height * 2) / 3;

          // assign the targeted size for the canvas holding the snapshot
          capturedImage.width = width;
          capturedImage.height = height;
          // compute the offset X and Y on the source
          const offsetX = (tmpImage.width - width) / 2;
          const offsetY = (height - height) / 2;
          // copy the cropped image to the target
          capturedImageCtx.drawImage(tmpImage, offsetX, offsetY, width, height, 0, 0, width, height);
          // also copy in the thumbnail
          thumbnailCtx.drawImage(tmpImage, offsetX, offsetY, width, height, 0, 0, 240, 360); // Or at whatever offset you like

          // save the cropped 2/3 image
          let format = 'image/jpeg';
          let quality = 0.75;
          self.image['dataImg'] = capturedImageCtx.canvas.toDataURL(format, quality);
          self.image['imageWidth'] = capturedImage.width;
          self.image['imageHeight'] = capturedImage.height;
          self.image['imageId'] = uuid.v4();

          // hides the capture/upload form component
          self.showVideo = false;
          // and show the form to add metadata to the image
          self.showForm = true;
          self.showFormUpload = false;
          self.showFormCapture = true;

          // Stop all video streams.
          if (player !== null && player.srcObject !== null) {
            (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
              track => track.stop()
            );
          }
        }

      }
    }

    // completed the upload, turn off the spinner
    this.showSpinner = false;
  }

  /**
   *
   */
  switchCamera() {
    const player = <HTMLVideoElement>document.getElementById('player');

    // Stop all video streams
    if (player !== null && player.srcObject !== null) {
      (<MediaStream>(player.srcObject)).getVideoTracks().forEach(
        track => track.stop()
      );
    }

    this.currentCamera = ((this.currentCamera + 1) % this.videoselectors.length);
    const constraints = {
      video: {deviceId: this.videoselectors[this.currentCamera].value ? {exact: this.videoselectors[this.currentCamera].value} : undefined}
    };
    navigator.mediaDevices.getUserMedia(constraints).then(
      (stream) => {
        // window.stream = stream; // make stream available to console
        const player = <HTMLVideoElement>document.getElementById('player');
        player.srcObject = stream;
        // this.mediaStreamTrack = window.URL.createObjectURL(stream); // mediaStream.getVideoTracks()[0];
        this._resizeVideoBorder();
      }).catch(this._handleError);
  }


  //
  // UI CALLBACKS
  //


  onInputChangeDocH(event: any) {
    this.document.documentHeight = event.value;
  }

  onInputChangeDocW(event: any) {
    this.document.documentWidth = event.value;
  }

  onInputChangeWatH(event: any) {
    this.document.watermarkHeight = event.value;
  }

  onInputChangeWatW(event: any) {
    this.document.watermarkWidth = event.value;
  }

  onWindowResize(event: any) {
    // console.log('event.target X ' + event.target.innerWidth + ' Y ' + event.target.innerHeight);
    this._resizeVideoBorder();
  }


  //
  // PIVATE METHODS
  //

  /**
   *
   * @param {string} value
   * @returns {string[]}
   * @private
   */
  private _filter(value: string): string[] {
    console.log('filter with ' + value + ' current institution ' + this.document.institution);
    const filterValue = value.toLowerCase();
    return this.institutions.filter(institution => institution.toLowerCase().includes(filterValue));
  }

  /**
   *
   * @private
   */
  private _resizeVideoBorder() {
    // width/height aspect ration for the capture photo
    let target_ration = 0.75
    // area for the filigrane wrt doc - 2 would be 50%, lower bigger (up to 1
    let target_area = 1.25
    let border = document.getElementById("border");

    let currentX = border.offsetWidth;
    let currentY = border.offsetHeight;
    let ratio = (currentX / currentY);

    if (ratio < target_ration) {
      currentY = Math.round((currentX / target_ration));
    } else {
      currentX = Math.round((currentY * target_ration));
    }

    let filigraneAreaX = Math.round(currentX / target_area);
    let filigraneAreaY = Math.round(currentY / target_area);

    // each border will be half of the remaining
    let borderX = Math.round((border.offsetWidth - filigraneAreaX) / 2);
    let borderY = Math.round((border.offsetHeight - filigraneAreaY) / 2);

    // use angular renderer to set style attributes
    this.renderer.setStyle(border, 'border-left-width', borderX + 'px');
    this.renderer.setStyle(border, 'border-right-width', borderX + 'px');
    this.renderer.setStyle(border, 'border-top-width', borderY + 'px');
    this.renderer.setStyle(border, 'border-bottom-width', borderY + 'px');
  }

  /**
   *
   * @param deviceInfos
   * @private
   */
  private _gotDevices(deviceInfos) {
    this.videoselectors = [];
    // Handles being called several times to update labels. Preserve values.
    for (let i = 0; i !== deviceInfos.length; ++i) {
      const deviceInfo = deviceInfos[i];
      const option = {};
      option['value'] = deviceInfo.deviceId;
      if (deviceInfo.kind === 'videoinput') {
        option['text'] = deviceInfo.label; // || `camera ${videoselectors.length + 1}`;
        this.videoselectors.push(option);
      } else {
        console.log('Some other kind of source/device: ', deviceInfo);
      }
    }
  }

  /**
   *
   * @param error
   * @private
   */
  private _handleError(error) {
    console.log('navigator.getUserMedia error: ', error);
  }

  // Playing event
  private _isPlaying(event) {
    // indirectly will trigger a redraw of the border on top of the video the video
    window.dispatchEvent(new Event('resize'));
  };


}

