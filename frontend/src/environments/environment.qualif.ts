export const environment = {
  production: true,
  serviceWorkerScript: 'sw-master.js',
  backendServerUrl: "https://filigranes-qualif.paris.inria.fr/api",

  //
  enable_registration: false,
};
