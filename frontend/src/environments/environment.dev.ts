export const environment = {
  production: false,
  serviceWorkerScript: 'sw-master.js',
  backendServerUrl: "http://127.0.0.1:9200/api",

  //
  enable_registration: false,
};
