(function () {

  // if not loaded yet, import the script
//  if (typeof self.idbKeyval === "undefined") {
    self.importScripts('/assets/js/idb-keyval-iife.js');
//  }

  // create or get handle on the indexedb for caching REST requests
  const customStore = new self.idbKeyval.Store('apirequests_dbname', 'apirequests_storename');

  // store request result in DB
  async function storaContent (key, value) {
    await self.idbKeyval.set(key, value, customStore);
    return;
  }

  // fetch request result from DB
  async function retrieveContent (key) {
    let str = await self.idbKeyval.get(key, customStore);
    return str;
  }

  // serialize the headers
  function serializeHeaders (headers) {
    let serialized = {}
    for (let entry of headers.entries()) {
      serialized[entry[0]] = entry[1]
    }
    return serialized
  }

  // serialize the request
  async function serializeRequest (request) {
    var serialized = {
      url: request.url,
      headers: serializeHeaders(request.headers),
      method: request.method,
      mode: request.mode,
      credentials: request.credentials,
      cache: request.cache,
      redirect: request.redirect,
      referrer: request.referrer,
    }

    // Only if method is not `GET` or `HEAD` is the request allowed to have body.
    if (request.method !== 'GET' && request.method !== 'HEAD') {
      let body = await request.clone().text();
      serialized.body = body
    }

    let resStr = JSON.stringify(serialized);
    return serialized;
  }

  // serialize the response
  async function serializeResponse (response) {
    let serialized = {
      headers: serializeHeaders(response.headers),
      status: response.status,
      statusText: response.statusText,
    }
    let body = await response.clone().text();
    if (body !== null && body !== undefined) {
      serialized.body = body
    }
    let resStr = JSON.stringify(serialized);
    return resStr;
  }

  // deserialize the response
  function deserializeResponse (data) {
    let newResponse = new Response(data.body, data);
    return newResponse;
  }

  // put request/response in cache
  async function cachePut (request, response, store) {
    let postId = await getPostId(request.clone());
    if (postId !== null && postId !== undefined) {
      let serializedResponse = await serializeResponse(response.clone());
      if (serializedResponse !== null && serializedResponse !== undefined) {
        await storaContent(postId, serializedResponse);
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  // try to find cached resposne for a request
  async function cacheMatch (request) {
    let postId = await getPostId(request.clone());
    if (postId !== null && postId !== undefined) {
     let cachedContent = await retrieveContent(postId);
     if (cachedContent !== null && cachedContent !== undefined) {
        return deserializeResponse(JSON.parse(cachedContent));
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  // generate unique ID from request
  async function getPostId (request) {
    let requestSerialized = await serializeRequest(request.clone())
    let postid = JSON.stringify(requestSerialized);
    return postid;
  }

  //
  //
  //
  //
  //

  self.addEventListener('notificationclick', (event) => {
    console.log('notification details: ', event.notification)
    event.notification.close()
  })

  // Listen to fetch requests
  self.addEventListener('fetch', function (event) {
    /*
    console.log('XXX PGR FETCH REQUEST clientId  ' +
      JSON.stringify(event.clientId))
    console.log('XXX PGR FETCH REQUEST request credentials  ' +
      JSON.stringify(event.request.credentials))
    console.log('XXX PGR FETCH REQUEST request destination  ' +
      JSON.stringify(event.request.destination))
    console.log('XXX PGR FETCH REQUEST request url  ' +
      JSON.stringify(event.request.url))
    console.log('XXX PGR FETCH REQUEST request body  ' +
      JSON.stringify(event.request.body))
    console.log('XXX PGR FETCH REQUEST request method ' +
      JSON.stringify(event.request.method))
    */

    // Exit early if we don't have access to the client.
    // Eg, if it's cross-origin.
    //if (!event.clientId) return;

    // Get the client.
    // const client = await clients.get(event.clientId);
    // Exit early if we don't get the client.
    // Eg, if it closed.
    // if (!client) return;


    if (event.request.method === 'GET') {
      if (event.request.url.includes('/api')) {
        // First try to fetch the request from the server
        event.respondWith(
          fetch(event.request.clone()).then(
            function (response) {
              cachePut(event.request.clone(), response.clone(), null);
              return response
            },
          ).catch(
            function () {
              let cachedResponse = cacheMatch(event.request.clone(), null);
              if (cachedResponse !== null && cachedResponse !== undefined) {
                // Send a message to the client.
                self.clients.matchAll().then(function (clients) {
                  clients.forEach(function (client) {
                    client.postMessage({
                      category: 'toast',
                      title: 'Warning',
                      msg: 'Unable to query server, using cached version instead',
                      url: event.request.url,
                    })
                  })
                })
                return cachedResponse;
              } else {
                // Send a message to the client.
                self.clients.matchAll().then(function (clients) {
                  clients.forEach(function (client) {
                    client.postMessage({
                      category: 'toast',
                      title: 'Warning',
                      msg: 'Service Unavailable',
                      url: event.request.url,
                    })
                  })
                })
                return new Response('', {status: 503, statusText: 'Service Unavailable'})
              }
            },
          ),
        )
      } else {
        // not a /api call, do nothing (continue propagation
      }
    }

    if (event.request.method === 'POST') {
      console.log('XXX PGR FETCH POST')

      if (event.request.url.includes('/api/image')) {
        // First try to fetch the request from the server
        event.respondWith(
          fetch(event.request.clone()).then(
            function (response) {
              cachePut(event.request.clone(), response.clone(), null);
              return response
            },
          ).catch(
            function () {
              // cache the response and return a fake
              let cachedResponse = cacheMatch(event.request.clone(), null);
              if (cachedResponse !== null && cachedResponse !== undefined) {
                // Send a message to the client.
                self.clients.matchAll().then(function (clients) {
                  clients.forEach(function (client) {
                    client.postMessage({
                      category: 'toast',
                      title: 'Warning',
                      msg: 'Unable to query server, using cached version instead',
                      url: event.request.url,
                    })
                  })
                })
                return cachedResponse;
              } else {
                // Send a message to the client.
                self.clients.matchAll().then(function (clients) {
                  clients.forEach(function (client) {
                    client.postMessage({
                      category: 'toast',
                      title: 'Warning',
                      msg: 'Service Unavailable',
                      url: event.request.url,
                    })
                  })
                })
                return new Response('', {status: 503, statusText: 'Service Unavailable'})
              }
            },
          ),
        )
      } else {
        // not a /api call, do nothing (continue propagation
      }
    }

  })

}())

