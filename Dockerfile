# Use an official Ubuntu runtime as a parent image
FROM ubuntu:16.04

# silence error messages
ENV TERM linux

# We want to use bash instead of shell
SHELL ["/bin/bash", "-c"]

# General installs in the docker
RUN apt-get -y update
RUN apt-get -y dist-upgrade

RUN apt-get -y install build-essential apt-utils software-properties-common

RUN apt-get install -y curl \
	git \
	screen \
	telnet \
	tmux \
	ufw \
        vim \
	wget

RUN apt-get install -y freeglut3-dev \
	g++ \
        libgtk2.0-dev \
	libx11-dev

# Install miniconda and add to PATH
RUN wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
RUN sh Miniconda3-latest-Linux-x86_64.sh -b
# RUN yes "yes" | bash Miniconda3-latest-Linux-x86_64.sh
RUN source /root/.bashrc
ENV PATH /root/miniconda3/bin:$PATH

# Python
RUN apt-get install -y python3-dev python3-pip
RUN pip3 install --upgrade pip

# Install Python libraries
RUN pip3 install dominate \
        https://download.pytorch.org/whl/cu100/torch-1.0.1.post2-cp35-cp35m-linux_x86_64.whl \
        numpy \
        flask \
        flask-cors \
        scikit-image \
        opencv-python \
        tqdm \
        ujson

RUN pip install flask \
        flask-cors

RUN conda install pytorch torchvision cudatoolkit=10.0 -c pytorch

# Add the igithub hosts as a known host to avoid interactive input with ssh
# NOT SECURE - should chezck fingerprint before adding to avoid MITM - see SO
RUN mkdir ~/.ssh
RUN ssh-keyscan -H github.com  >> ~/.ssh/known_hosts

# copy the deploy key in docker (should be created/added in the github repop settings also
ADD ./fpt_watermarks_id_rsa /root/.ssh/fpt_watermarks_id_rsa

# add key to ssh agent and then clone the repo (in the same command/shell session)
# uses git and not https to use ssh key
RUN eval "$(ssh-agent -s)" && \
        ssh-add /root/.ssh/fpt_watermarks_id_rsa && \
        git clone git@github.com:oumayb/watermarks.git


# expose ports for flask app
EXPOSE 5002

# temp install
RUN pip install scikit-image
RUN conda install opencv
RUN pip install tqdm ujson

# launch apps
RUN cd watermarks/WaterMarkRecognition/localMatching/ && \
        eval "$(ssh-agent -s)" && \
        ssh-add /root/.ssh/fpt_watermarks_id_rsa && \
        git pull && \
	python3 save_search_features.py --label_path ../data/labels_1000.json --save_dir search_feat_1000 && \
        python3 app.py
