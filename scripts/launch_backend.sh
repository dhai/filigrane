#!/bin/sh
cd /home/gitlab-runner/deploy/backend/app
# kill -9 $(ps -ef | grep 'node app.js' | grep -v grep | awk '{print $2}')
# setsid nohup node app.js &> nohup.out &
export NODE_ENV=production && node app.js
# exit 0
