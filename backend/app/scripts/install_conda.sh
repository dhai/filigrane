#!/bin/bash

# install conda
curl -O https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
sh Anaconda3-2019.03-Linux-x86_64.sh

# create env for DB import scripts
# if failure maybe conda version issue (works with 4.6.11 on macosx)
# required modules for db upload are : pymongo numpy requests lxml unidecode
conda create -n py37 python=3.7 --file requirements.yml
# activazte env
conda activate py37

