'use strict'

// Usage:
//    node uploadDatabase.js  --help
//    node upload_minutier.js --userid ilaria --separator , --csv ./metadata_minutier.csv --imgsrc ./data/photo/minutier/
//

console.log('config environment : ' + process.env.NODE_ENV)
const config = require('../config/config.js')[process.env.NODE_ENV ||
'development']

// command line processing
const commandLineArgs = require('command-line-args')
const commandLineUsage = require('command-line-usage')

// CSV parser
const fs = require('fs')
const csv = require('csv-parser')
// separator - can be changed
let SEPARATOR = ';'

// generate unique ID
const uuidv4 = require('uuid/v4')

// image manipulation library
const sharp = require('sharp')
// to get image size from buffer
const probe = require('probe-image-size')

// Create a connection to MongoDB
const dbconfig = require('../config/database')
const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
mongoose.connect(dbconfig.database, {promiseLibrary: require('bluebird')}).
  then(() => {
    console.log('mongodb connection succesful to ' + dbconfig.database)
  }).
  catch((err) => console.error('mongodb error' + err))

//
// Data models
//
const Image = require('../models/Image')
const Thumbnail = require('../models/Thumbnail')
const Document = require('../models/Document').Document
const Page = require('../models/Document').Page

const timer = ms => new Promise(res => setTimeout(res, ms))

//
//
//

const DB_NAME_MINUTIER = 'minutier';

const INSTIUTUTION_NAME = 'Archives Nationales - Minutier central';

const HEADER_TITLE_IMGSRC = 'Dossier';
const HEADER_TITLE_TPQ = 'Date post quem';
const HEADER_TITLE_TAQ = 'Date ante quem';
const HEADER_TITLE_REFERENCE = 'References';
const HEADER_TITLE_SHAPE = 'Motif';

const HEADER_ID_TPQ = 'TPQ';
const HEADER_ID_TAQ = 'TAQ';
const HEADER_ID_STUDY = 'STUDY';
const HEADER_ID_SHAPE = 'SHAPE';

const HEADER_ID_REFERENCE = 'REFERENCE';

const DEFAULT_THUMBNAIL_WIDTH = 100
const DEFAULT_THUMBNAIL_HEIGHT = 100

//
// command line options processing
//

class FileDetails {
  constructor (filename) {
    this.filename = filename
    this.exists = fs.existsSync(filename)
  }
}

// definition of CL parameters
const optionDefinitions = [
  {
    name: 'userid',
    alias: 'u',
    type: String,
  },
  {
    name: 'separator',
    alias: 's',
    type: String,
  },
  {
    name: 'csv',
    alias: 'c',
    type: filename => new FileDetails(filename),
  },
  {
    name: 'imgsrc',
    alias: 'i',
    type: filename => new FileDetails(filename),
  },
  {
    name: 'help',
    alias: 'h',
    type: Boolean,
    description: 'Display this usage guide.',
  },
]

// definition of CL parameters help
const sections = [
  {
    header: 'Filigrane database import script',
    content: 'Use csv file and image folders to import entries in the filigrane database.',
  },
  {
    header: 'Options',
    optionList: [
      {
        name: 'userid',
        typeLabel: 'string',
        description: 'The login/ID of the user uploading the data',
      },
      {
        name: 'separator',
        typeLabel: 'string',
        description: 'Single character for the separator for the data in the CSV file (default is ;).',
      },
      {
        name: 'csv',
        typeLabel: '{underline file}',
        description: 'The file with the input data to upload in the filigrane database in csv format.',
      },
      {
        name: 'imgsrc',
        typeLabel: '{underline directory}',
        description: 'The root directory for the folders containing the images to upload.',
      },
      {
        name: 'help',
        description: 'Print this usage guide.',
      },
    ],
  },
]

// process CL
const options = commandLineArgs(optionDefinitions)
// console.log(options);

// if help or no CL parameters
if (options.help || ![options.imgsrc && options.userid && options.imgsrc]) {
  const usage = commandLineUsage(sections)
  console.log(usage)
  process.exit()
}
// error in image folder
if (options.imgsrc && !options.imgsrc.exists) {
  console.log('provide a valid directory for input images');
  process.exit();
}
// error in csv file
if (options.csv && !options.csv.exists) {
  console.log('provide a valid csv file for input data');
  process.exit();
}
// error in csv file
if (options.separator && options.separator.length !== 1) {
  console.log('provide a single character for the separator');
  process.exit();
}

// setup the separator for the csv file if defined
if (options.separator) {
  console.log('setup the csv file separator as ' + options.separator);
  SEPARATOR = options.separator;
}

// define default userid
if (options.userid === undefined) {
  options.userid = 'ilariap'
}

//
// parse CSV data
//

const results = [];

function importData () {

  console.log('\n\## Import data on behalf of user <' +
    JSON.stringify(options.userid) + '>');

  fs.createReadStream(options.csv.filename).
    pipe(csv({separator: SEPARATOR})).
    on('headers', (headers) => {
      console.log(`First header: ${headers[0]}`);
    }).
    on('data', (data) => {
      // console.log(`data:`)
      results.push(data);
    }).
    on('end', () => {
      //console.log(results);
      importResults(results);
    })
}

//
// process CSV data
//

async function importResults (data) {
  for (let row of data) {
    // console.log('\n\, IMPORT ROW ' + JSON.stringify(row) );
    await importSingleEntry(row);
  }

  console.log('allow all operations to complete');
  timer(3000).then(_ => {
    console.log('completed')
    // process.exit()
  })

}

/**
 *
 * @returns {*}
 */
function createDocument () {
  let newDoc = new Document();
  newDoc.documentId = '';
  newDoc.institution = '';
  newDoc.documentType = '';
  newDoc.documentWidth = 0;
  newDoc.documentHeight = 0;
  newDoc.watermarkWidth = 0;
  newDoc.watermarkHeight = 0;
  newDoc.watermarkShape = '';
  newDoc.totalPages = 0;
  newDoc.description = '';
  newDoc.keywords = '';
  newDoc.TPQ = 0;
  newDoc.TAQ = 0;
  newDoc.allowPhotoRepro = true;
  newDoc.allowPhotoTrain = true;
  newDoc.pages = [];
  newDoc.updatedDate = new Date();
  newDoc.parameters = [];

  return newDoc
}

/**
 *
 * @param doc
 * @param itemdata
 */
function initDocument (doc, itemdata) {

  doc.userId = options.userid;

  doc.TPQ = itemdata[HEADER_TITLE_TPQ];
  doc.TAQ = itemdata[HEADER_TITLE_TAQ];
  doc.watermarkShape = itemdata[HEADER_TITLE_SHAPE];

  doc.institution = INSTIUTUTION_NAME;
  doc.documentId = 'photo' + '-' + DB_NAME_MINUTIER + '-' + itemdata[HEADER_TITLE_IMGSRC];

  {
    let tmpParameterDoc = {
      category: 'EXTRA',
      value: itemdata[HEADER_TITLE_REFERENCE],
      name: HEADER_ID_REFERENCE,
    }
    doc.parameters.push(tmpParameterDoc);
  }

  return doc;
}

function createPage () {
  let tmpPage = new Page();
  tmpPage.pageId = uuidv4();
  tmpPage.pageNumber = 0;
  tmpPage.writingOrigin = '';
  tmpPage.writingDate = '';

  tmpPage.documentWidth = 0;
  tmpPage.documentHeight = 0;
  tmpPage.watermarkWidth = 0;
  tmpPage.watermarkHeight = 0;

  tmpPage.thumbnails = []
  tmpPage.parameters = []

  return tmpPage
}

/**
 *
 * @param page
 * @param newImageId
 * @param pageCounter
 * @param itemdata
 * @returns {*}
 */
function initPage (page, newImageId, pageCounter, itemdata) {
  page.pageNumber = pageCounter;
  {
    let thumbnail = {
      imageId: newImageId,
      imageWidth: DEFAULT_THUMBNAIL_WIDTH,
      imageHeight: DEFAULT_THUMBNAIL_HEIGHT,
    }
    page.thumbnails.push(thumbnail);
  }
  return page;
}


async function processImageDile (filename, pageCounter, itemdata) {
  // load the image and save in blob format at a given quality
  //console.log('save image')
  return sharp(filename).
    jpeg({quality: 70}).
    toBuffer('base64').
    then(imgdata => {
      const tmpImageInfo = probe.sync(imgdata);

      // create image object
      // make sure proper user ID is used
      // assign a new unique ID for the image
      let newImage = new Image();
      newImage.imageId = uuidv4();
      newImage.imageWidth = tmpImageInfo.width;
      newImage.imageHeight = tmpImageInfo.height;
      newImage.dataImg = 'data:image/jpg;base64,' +
        imgdata.toString('base64');

      // will save async, hope ok
      let newImageResult = Image.create(newImage);

      // TODO
      // should make sure this imageId does not exist
      let newThumbnail = {
        imageId: newImage.imageId,
        imageWidth: 240,
        imageHeight: 320,
      }
      newThumbnail.dataImg = sharp(imgdata).resize(240, 320).toBuffer().
        then(data => {
          newThumbnail.dataImg = 'data:image/jpg;base64,' + Buffer.from(data).toString('base64') ;
          Thumbnail.create(newThumbnail)
        }).catch(err => {
          console.log('failed to generate thumbnail ' + err)
        });


      //if (err) {
      //  console.log('Error when saving image ' + err)
      //}
      console.log('image created, create page')
      let tmpPage = createPage()
      tmpPage = initPage(tmpPage, newImage.imageId, pageCounter, itemdata)
      return tmpPage
    })
}

/**
 *
 * @param itemdata
 */
async function importSingleEntry (itemdata) {

  // test that image foler name is provided
  if (itemdata[HEADER_TITLE_IMGSRC] === null ||
    itemdata[HEADER_TITLE_IMGSRC] === undefined ||
    itemdata[HEADER_TITLE_IMGSRC] === '') {
    // console.log('row with no folder provided for images, skip')
    return;
  }

  const foldername = options.imgsrc.filename + '/' + itemdata[HEADER_TITLE_IMGSRC]

  try {
    if (!fs.lstatSync(foldername).isDirectory()) {
      // console.log('row with folder name but not proper directory, skip')
      return;
    }
  } catch (e) {
    // console.log('exception while testing image directory ' + e)
    return;
  }

  let newDoc = createDocument();
  newDoc = initDocument(newDoc, itemdata);

  try {
    // cannot break with forEach, use some/every to break (either return true or false to break
    // https://stackoverflow.com/questions/6260756/how-to-stop-javascript-foreach#6260865
    let imagefiles = fs.readdirSync(foldername);
    // console.log('\n files are ' + JSON.stringify(imagefiles));

    // initialize page counter
    let docPageCounter = 1;

    for (let file of imagefiles) {
      // also suffiw should be a known/supported image format
      const suffix = file.toLowerCase().substr(file.length - 3);
      if (suffix === 'png' || suffix === 'jpg' || suffix === 'gif') {

        // get the complete image filename
        const filename = foldername + '/' + file;
        console.log('page for image filename ' + filename);
        try {
          let tmpPage = await processImageDile(filename, docPageCounter, itemdata);
          if (tmpPage === null || tmpPage === undefined) {
            console.log('ERRO, page is empty');
          } else {
            //console.log('ADDING new page ' + JSON.stringify(tmpPage));
            newDoc.pages.push(tmpPage);
            docPageCounter++;
          }
        } catch (e) {
          console.log('exception while processing image' + e);
        }
      }
    }

    // now save the document with all the pages info
    newDoc.totalPages = (docPageCounter - 1);
    // console.log('save document : ' + JSON.stringify(newDoc));
    Document.create(newDoc, function (err, post) {
      if (err) {
        console.log('Error when saving document ' + err);
      } else {
        console.log('document saved ');
      }
    })

  } catch (exception) {
    console.log('exception 3 : ' + exception);
  }
}

//
// STARTS THE DATA UPLOAD AFTER 3 SEC
//

console.log('wait a few seconds');
timer(3000).then(_ => {
  console.log('upload data now');
  importData();
})
