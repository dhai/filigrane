import argparse
import base64
import datetime
from pathlib import Path
import requests
from tqdm import tqdm
import uuid

import numpy as np
from lxml import html
from pymongo import MongoClient
from unidecode import unidecode


def get_files_from_dir(dir_path, valid_extensions=None, recursive=False, sort=False):
    path = Path(dir_path)
    if recursive:
        files = [f.absolute() for f in path.glob('**/*') if f.is_file()]
    else:
        files = [f.absolute() for f in path.glob('*') if f.is_file()]

    if valid_extensions is not None:
        valid_extensions = [valid_extensions] if isinstance(valid_extensions, str) else valid_extensions
        valid_extensions = ['.{}'.format(ext) if not ext.startswith('.') else ext for ext in valid_extensions]
        files = list(filter(lambda f: f.suffix in valid_extensions, files))

    return sorted(files) if sort else files


def get_watermark_info(idx):
    try:
        url = 'http://www.ksbm.oeaw.ac.at/_scripts/php/loadRepWmark.php?rep=briquet&refnr={}&lang=fr'.format(idx)
        response = requests.get(url)
        response.raise_for_status()
        e = html.fromstring(response.content)
        info = sum(map(lambda s: s.split(),
                       unidecode(' '.join(e.xpath('//td[@class="wm_text"]/text()'))).strip().split('\n')), [])
        w, h = info[info.index('w') + 1], info[info.index('h') + 1]
        shape = ' '.join(info[info.index('Motif:') + 1:])
        img_info = unidecode(e.xpath('//td[@class="imgsize"]/text()')[0]).split()
        img_w, img_h = img_info[-4], img_info[-2]
        try:
            date = e.xpath('//th/div/div/text()')[1].split()[1]
        except IndexError as index_err:
            print('get_watermark_info error, idx={}'.format(idx), index_err)
            date = ''
        return {'id': idx, 'shape': shape, 'width': w, 'height': h, 'img_width': img_w, 'img_height': img_h,
                'date': date, 'url': url}
    except (requests.exceptions.RequestException, requests.exceptions.HTTPError, ValueError) as e:
        print('get_watermark_info error, idx={}'.format(idx), e)
        return {'id': idx, 'shape': 'Not found', 'width': 0, 'height': 0, 'img_width': 0, 'img_height': 0, 'date': 0, 'url': ''}


if __name__ == '__main__':
    try:
        conn = MongoClient()
        print("Connected successfully!!!")
    except BaseException as e:
        print("Could not connect to MongoDB: {}".format(e))
    else:
        # database
        db = conn.filigranes

        # Get 3 collections we want to update
        doc_collection = db.documents
        thumb_collection = db.thumbnails
        images_collection = db.images

        parser = argparse.ArgumentParser()
        parser.add_argument('--img_dir', type=str, help="path to the images we're importing")
        args = parser.parse_args()

        img_dir = args.img_dir
        db_name = Path(img_dir).name
        image_files = get_files_from_dir(img_dir, valid_extensions='png', recursive=True)

        # Lists to keep track of added Objects (the ids will be the ids assigned to the db as ObjectId)
        added_image_ids = []
        added_thumbnails_ids = []
        added_documents_ids = []

        for img_file in tqdm(image_files):
            encoded = base64.b64encode(open(str(img_file), 'rb').read())

            # Add Image
            id_image = str(uuid.uuid4())
            img = {'dataImg': 'data:image/jpeg;base64,' + encoded.decode("utf-8"),
                   'imageWidth': 256,
                   'imageHeight': 256,
                   'imageId': id_image
                   }

            img_1 = images_collection.insert_one(img)
            image_id = img_1.inserted_id
            added_image_ids.append(image_id)

            # Add thumbnail
            thumbnail = {
                'imageId': id_image,
                'imageWidth': 256,
                'imageHeight': 256,
                'dataImg': 'data:image/jpeg;base64,' + encoded.decode("utf-8")
            }

            thumb_1 = thumb_collection.insert_one(thumbnail)
            thumb_id = thumb_1.inserted_id
            added_thumbnails_ids.append(thumb_id)

            # Add document
            info = get_watermark_info(img_file.parent.name)
            document = {
                'parameters': [],
                'pages': [{
                    'thumbnails': [{'_id': thumb_id,
                                    'imageWidth': 256,
                                    'imageHeight': 256,
                                    'imageId': id_image}],
                    'parameters': [],
                    'documentWidth': 0,
                    'documentHeight': 0,
                    'watermarkWidth': 0,
                    'watermarkHeight': 0,
                    'pageNumber': 1}],
                'documentWidth': info['img_width'],
                'documentHeight': info['img_height'],
                'watermarkWidth': info['width'],
                'watermarkHeight': info['height'],
                'allowPhotoRepro': True,
                'allowPhotoTrain': True,
                'institution': 'Briquet - catalogue',
                'reference': info['id'],
                'parameters': [{'category': 'EXTRA','value': info['url'],'name': 'URL'}],
                'watermarkShape': info['shape'],
                'TAQ': info['date'],
                'TPQ': info['date'],
                'documentType': 'dessin',
                'totalPages': 1,
                'documentId': 'drawing-{}-{}'.format(db_name, info['id']),
                'userId': 'oumayma',
                'updatedDate': str(datetime.datetime.now()),
                '__v': 0
            }

            doc_1 = doc_collection.insert_one(document)
            doc_id = doc_1.inserted_id
            added_documents_ids.append(doc_id)

        # Keep track of uploaded ObjectIds
        np.save(img_dir + "documents_ids.npy", np.array(added_documents_ids))
        np.save(img_dir + "thumbnails_ids.npy", np.array(added_thumbnails_ids))
        np.save(img_dir + "images_ids.npy", np.array(added_image_ids))
