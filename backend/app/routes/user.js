//
//
//
//
//

const mongoose = require('mongoose')
const passport = require('passport')
const dbconfig = require('../config/database')
const config = require('../config/config.js')[process.env.NODE_ENV ||
'development']
require('../config/passport')(passport)

const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()

const User = require('../models/User')

//
// setup all routes for User resources
//

// disable registration on backend side if not required
if (config.enable_registration) {
  router.post('/signup', function (req, res) {
    console.log('API signup')
    if (!req.body.username || !req.body.password) {
      console.log('signup : invalid parameters')
      res.json({success: false, msg: 'Please pass username and password.'})
    } else {
      let newUser = new User({
        username: req.body.username,
        lastname: req.body.lastname,
        firstname: req.body.firstname,
        affiliation: req.body.affiliation,
        email: req.body.email,
        password: req.body.password,
      })
      // save the user
      newUser.save(function (err) {
        if (err) {
          console.log('signup :ERROR already exist')
          return res.json({success: false, msg: 'Username already exists.'})
        }
        console.log('signup :OK new user')
        res.json({success: true, msg: 'Successful created new user.'})
      })
    }
  })
}


router.post('/signin', function (req, res) {
  console.log('API signin')
  User.findOne({
    username: req.body.username,
  }, function (err, user) {
    if (err) {
      console.log('signin : exception while looking for user ' + err)
      throw err
    }

    if (!user) {
      console.log('signin :Authentication failed. User not found')
      res.status(401).
        send({success: false, msg: 'Authentication failed. User not found.'})
    } else {
      // check if password matches
      console.log('signin : user found check password')
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          console.log('signin : Authentication ok, return JWT')

          // if user is found and password is right create a token
          let token = jwt.sign(user.toJSON(), dbconfig.secret)

          // return the information including token as JSON
          res.json({success: true, token: 'JWT ' + token})
        } else {
          console.log('signin : Authentication failed')
          res.status(401).
            send(
              {success: false, msg: 'Authentication failed. Wrong password.'})
        }
      })
    }
  })
})

/**
 * Retrieves token from message headers.
 * Assumes second part of the authorization header
 *
 * @param headers
 * @returns {*}
 */
function getToken (headers) {
  if (headers && headers.authorization) {
    const parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

//
//
//

module.exports = router
