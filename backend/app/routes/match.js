//
//
//
//
//

const mongoose = require('mongoose')
const passport = require('passport')
// load config.js
var config;
try {
  config = require('../config/config.js')[process.env.NODE_ENV || 'development'];
} catch (e) {
  handleError(new Error('Cannot find config.js'));
}
require('../config/passport')(passport)

const https = require('https')
const http = require('http')

const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()

const User = require('../models/User')
const Document = require('../models/Document.js').Document

//
// setup all routes for User resources
//

router.post('/photo', function (req, res) {
  if (!req.body.dataImg) {
    console.log('photo : invalid parameters')
    res.json({success: false, msg: 'Please provide dataImg.'})
  } else {
    let matchPhoto = {
      dataImg: req.body.dataImg
    }
    try {
      doMatch(req.body, res);
    } catch (err) {
      console.log('error when calling search backend ' + err);
    }
  }
})

/**
 * Returns a list of random IDs from the ones in the database
 *
 * @param matchPhoto
 * @param res
 * @returns {Promise.<void>}
 * /
async function doMatchPhotoRandom( matchPhoto , res) {
  'use strict'

  try {
    // Get the count of all users
    const count = await Document.count().exec();
    const matchingDocIds = [];

    const randomMatching = Math.floor(Math.random() * 5) + 1;

    for (let cpt = 0; cpt < randomMatching; cpt++) {
      const random = Math.floor(Math.random() * count)
      // Again query all users but only fetch one offset by our random #
      const matchingDoc = await Document.findOne().skip(random).exec();
      matchingDocIds.push(matchingDoc.documentId);

    }
    console.log(matchingDocIds);
    // send doc id list as response
    res.json({success: true, msg: matchingDocIds})
  } catch (err) {
    // console.log('match :ERROR unable to proceed');
    // send back error message
    return res.json({success: false, msg: 'match :ERROR unable to proceed'})
  }
}
*/
/**
 *
 * @param matchPhoto
 * @param response
 * @returns {Promise}
 */
async function doMatch( matchPhoto , response) {
  'use strict'

  console.log('doMatch 1 ')

  // prepares the request parameters
  const requestMatchingDrawingObj = {
    host: config.search_address,
    port: config.search_port,
    method: 'POST',
    path: config.search_path_drawing,
    headers: {
      'accept-encoding': 'utf-8',
      'Content-Type': 'application/json',
    },
  }

  // prepares the request parameters
  const requestMatchingPhotoObj = {
    host: config.search_address,
    port: config.search_port,
    method: 'POST',
    path: config.search_path_photo,
    headers: {
      'accept-encoding': 'utf-8',
      'Content-Type': 'application/json',
    },
  }

  console.log('doMatch 2')

  // prepares the body of the post request
  const bodyRequest = {
    'photo': matchPhoto
  }

  let responseObj = response;


  console.log('doMatch 3 requestMatchingDrawingObj ' + JSON.stringify(requestMatchingDrawingObj))
  console.log('doMatch 3 requestMatchingPhotoObj ' + JSON.stringify(requestMatchingPhotoObj))


  const promisePhoto = new Promise((resolve, reject) => {
    //
    let requestMatching = http.request(requestMatchingDrawingObj, function (res) {
      // the response comes in multi parts
      let body = ''

      // complete the response
      res.on('data', function (data) {
        body += data
      })

      // fetching response completed, process the response
      res.on('end', function () {

        try {
          // parse the response, and retrieve the array of IDs
          const resp = JSON.parse(body)
          const documentMatchIds = resp['doc_ids'];
          const documentBriquetIds = resp['ids'];

          console.log('doMatchDrawingEngravingi response-1 '+ JSON.stringify(documentMatchIds) )
          console.log('doMatchDrawingEngravingi response-2 '+ JSON.stringify(documentBriquetIds) )

          // returns the array in a response object
          // response.json({success: true, msg: {'match_type': 'photo', 'docIds':documentMatchIds, 'ids':documentBriquetIds}})

          //return {success: true, msg: {'match_type': 'drawing', 'docIds':documentMatchIds, 'ids':documentBriquetIds}}
          resolve({success: true, msg: {'match_type': 'drawing', 'docIds':documentMatchIds, 'ids':documentBriquetIds}})

        } catch (e) {
          // returns an error response object
          // response.json({success: false, msg: ""})
          return {success: false, msg: ""}
          reject( {success: false, msg: ""})

        }

        // complete the promise
        console.log('doMatchPhotoEngravingi response-3' )
        // resolve({success: true, msg: {'match_type': 'drawing', 'docIds':documentMatchIds, 'ids':documentBriquetIds}})
      })

      res.on('error', function (error) {
        // returns an error response object
        // response.json({success: false, msg: ""})

        console.log('Got error: ' + error.message)
        reject(error)
      })
    })

    // check that socket can be opened, if not abort the request
    requestMatching.on('socket', function (socket) {
      socket.on('timeout', function () {
        // unable to connect, abort the request
        console.log('Error socket timeout, abort it')
        requestMatching.abort()
      })
    })

    // requestet aborted/failed, end on error
    requestMatching.on('error', (error) => {
      // returns an error response object
      // response.json({success: false, msg: ""})
      console.log('Request error: ' + error)
      reject(error)
    })

    // complete the request preparation & start it
    var bodyStr = JSON.stringify(bodyRequest)
    requestMatching.write(bodyStr)
    requestMatching.end()
  })

  console.log('doMatch 4 ')

  const promiseDrawing = new Promise((resolve, reject) => {
      //
      let requestMatching = http.request(requestMatchingPhotoObj, function (res) {
        // the response comes in multi parts
        let body = ''

        // complete the response
        res.on('data', function (data) {
          body += data
        })

        // fetching response completed, process the response
        res.on('end', function () {

          try {
            // parse the response, and retrieve the array of IDs
            const resp = JSON.parse(body)
            const documentMatchIds = resp['doc_ids'];
            const documentBriquetIds = resp['ids'];

            console.log('doMatchPhotoEngravingi response-1 '+ JSON.stringify(documentMatchIds) )
            console.log('doMatchPhotoEngravingi response-2 '+ JSON.stringify(documentBriquetIds) )

            // returns the array in a response object
            // response.json({success: true, msg: {'match_type': 'drawing', 'docIds':documentMatchIds, 'ids':documentBriquetIds}})

            // return {success: true, msg: {'match_type': 'photo', 'docIds':documentMatchIds, 'ids':documentBriquetIds}}
            resolve({success: true, msg: {'match_type': 'photo', 'docIds':documentMatchIds, 'ids':documentBriquetIds}})

          } catch (e) {
            // returns an error response object
            // response.json({success: false, msg: ""})
            // return {success: false, msg: ""}
            reject({success: false, msg: ""})

          }
          // complete the promise
          console.log('doMatchPhotoEngravingi response-3' )

        })

        res.on('error', function (error) {
          // returns an error response object
          // response.json({success: false, msg: ""})

          console.log('Got error: ' + error.message)
          reject(error)
        })
      })

      // check that socket can be opened, if not abort the request
      requestMatching.on('socket', function (socket) {
        socket.on('timeout', function () {
          console.log('Error socket timeout, abort it')

          // unable to connect, abort the request
          requestMatching.abort()
        })
      })

      // requestet aborted/failed, end on error
      requestMatching.on('error', (error) => {
        // returns an error response object
        response.json({success: false, msg: ""})

        console.log('Request error: ' + error)
        reject(error)
      })

      // complete the request preparation & start it
      var bodyStr = JSON.stringify(bodyRequest)
      requestMatching.write(bodyStr)
      requestMatching.end()
    })

  console.log('doMatch 5 ')

  await Promise.all( [promisePhoto, promiseDrawing]).then(function(values) {
    console.log('doMatch 6 ')
    console.log(values);
    responseObj.json({success: true, msg: values})
    return
  }).catch(err => console.log('error', err));

}

/**
 * Retrieves token from message headers.
 * Assumes second part of the authorization header
 *
 * @param headers
 * @returns {*}
 */
function getToken (headers) {
  if (headers && headers.authorization) {
    const parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

//
//
//

module.exports = router
