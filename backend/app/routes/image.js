//
//
//

const passport = require('passport')
require('../config/passport')(passport)

const express = require('express')
const router = express.Router()

const jwt = require('jsonwebtoken')
const uuidv4 = require('uuid/v4')

const sharp = require('sharp')

const mongoose = require('mongoose')
const Image = require('../models/Image')
const Thumbnail = require('../models/Thumbnail')

//
// setup all routes for User resources
//

/* GET SINGLE IMAGE BY ID */
router.get('/:id', function (req, res, next) {

      let currentUser = ''
      if (req.user !== undefined && req.user !== null) {
        currentUser = req.user.username
      }

      Image.findOne({
        imageId: req.params.id,
      }, function (err, image) {
        'use strict'
        if (err) return next(err)
        res.json(image)
      })
  })


/* SAVE IMAGE */
router.post('/', passport.authenticate('jwt', {session: false}),
  function (req, res, next) {

    const token = getToken(req.headers)
    if (token) {
      let currentUser = ''
      if (req.user !== undefined && req.user !== null) {
        currentUser = req.user.username
      }

      let newImage = req.body

      // make sure proper user ID is used
      newImage.userId = currentUser

      // let imageId = uuidv4()
      // newImage.imageId = imageId

      // TODO
      // should make sure this imageId does not exist
      let newThumbnail = {
        imageId: newImage.imageId,
        imageWidth: 240,
        imageHeight: 320,
      }

      let regex = /^data:.+\/(.+);base64,(.*)$/;
      let matches = newImage.dataImg.match(regex);
      let data = matches[2];
      let inputBuffer = new Buffer(data, 'base64');

      newThumbnail.dataImg = sharp(inputBuffer).resize(320, 320, {
        fit: sharp.fit.inside
      }).toBuffer().
        then(data => {
          newThumbnail.dataImg = 'data:image/jpg;base64,' + Buffer.from(data).toString('base64') ;

          Thumbnail.create(newThumbnail, function (err, resp) {
              if (err) {
                //
              } else {
                //
              }
            },
          )
        }).catch(err => {
          console.log('failed to generate thumbnail ' + err)
        });

      Image.create(newImage, function (err, post) {
        if (err) return next(err)
        res.json(post)
      })

    }
    else {
      res.status(403).send({success: false, msg: 'Unauthorized.'})
      return next(err)
    }
  },
)
//
//
//

function getToken (headers) {
  if (headers && headers.authorization) {
    const parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

module.exports = router
