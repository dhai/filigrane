//
//
//

const passport = require('passport')
require('../config/passport')(passport)

const express = require('express')
const router = express.Router()

const jwt = require('jsonwebtoken')
const uuidv4 = require('uuid/v4')

const sharp = require('sharp')

const mongoose = require('mongoose')
const Thumbnail = require('../models/Thumbnail')
const Image = require('../models/Image')

//
// setup all routes for User resources
//

/* GET SINGLE IMAGE BY ID */
router.get('/:id', function (req, res, next) {

      Thumbnail.findOne({
        imageId: req.params.id,
      }, function (err, image) {
        'use strict'
        // if no theumbnail, try to fetch actual image and generate thumbnail on the fly
        if (err || image === null || image === undefined) {

          Image.findOne({
            imageId: req.params.id,
          }, function (err, fullimage) {
            'use strict'
            // if no theumbnail, try to fetch actual image and generate thumbnail on the fly
            if (err || fullimage === null || fullimage === undefined) {
              // no image or thumbnail return error
              return next(err)
            } else {
              // generate thumbnail and save
              // should make sure this imageId does not exist

              let newThumbnail = {
                imageId: fullimage.imageId,
                imageWidth: 240,
                imageHeight: 320,
              }

              let regex = /^data:.+\/(.+);base64,(.*)$/;
              let matches = fullimage.dataImg.match(regex);
              let data = matches[2];
              let inputBuffer = new Buffer(data, 'base64');

              newThumbnail.dataImg = sharp(inputBuffer).resize(240, 320).toBuffer().
                then(data => {
                  newThumbnail.dataImg = 'data:image/jpg;base64,' + Buffer.from(data).toString('base64') ;
                  Thumbnail.create(newThumbnail, function (err, resp) {
                      if (err) {
                        console.log('failed to save thumbnail ' + err)
                        //
                      } else {
                        //
                      }
                    },
                  )
                  res.json(newThumbnail);
                }).catch(err => {
                  return next(err)
                });

            }
          });
        } else {
          res.json(image)
        }
      })
  })


//
//
//

function getToken (headers) {
  if (headers && headers.authorization) {
    const parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

module.exports = router
