//
//
//

const mongoose = require('mongoose')
const passport = require('passport')
require('../config/passport')(passport)

const express = require('express')
const router = express.Router()

const jwt = require('jsonwebtoken')

//var Filigrane = require('../models/Filigrane.js');
const Document = require('../models/Document.js').Document

const uuidv4 = require('uuid/v4')

//
//
//

//
// setup all routes for Filigrane resources
//

/* GET ALL DOCUMENTS - remove image */
// router.get('/', passport.authenticate('jwt', {session: false}),
router.get('/',
  function (req, res, next) {

//    const token = getToken(req.headers)
//    if (token) {
//      let currentUser = ''
//      if (req.user !== undefined && req.user !== null) {
//        currentUser = req.user.username
//      }

      // - potentially use .select('-Image') to exclude the dataimg field from the query directly
      // - return only user's contribution : 'userId': currentUser
      Document.find({}, function (err, alldocuments) {
        if (err) {
          return next(err)
        }
        res.json(alldocuments)
      })
//    } else {
//      res.status(403).send({success: false, msg: 'Unauthorized.'})
//      return next(err)
//    }
  })


/* GET SINGLE DOCUMENT BY ID */
router.get('/:documentId',
  function (req, res, next) {
    Document.findOne({documentId: req.params.documentId}, function (err, post) {
        if (err) {
          return next(err)
        }
        // console.log('found and return document with documentId ' + req.params.documentId + ' ' + JSON.stringify (post))
        res.json(post)
      })
  })

/* SAVE DOCUMENT */
router.post('/', passport.authenticate('jwt', {session: false}),
  function (req, res, next) {

    const token = getToken(req.headers)
    if (token) {
      let currentUser = ''
      if (req.user !== undefined && req.user !== null) {
        currentUser = req.user.username
      }

      let newDocument = req.body

      // make sure proper user ID is used
      newDocument.userId = currentUser

      // generate on client side
      // let documentId = uuidv4()
      // newDocument.documentId = documentId
      // for (let page of newDocument.pages) {
      //   page.pageId = uuidv4()
      // }

      // TODO
      // make sure all the photos linked in the doc are from the user
      // make sure the doc does not exist (not put)


      Document.create(newDocument, function (err, post) {
        if (err) return next(err)
        res.json(post)
      })

    } else {
      res.status(403).send({success: false, msg: 'Unauthorized.'})
      return next(err)
    }
  })

/* UPDATE DOCUMENT */
router.put('/:documentId', passport.authenticate('jwt', {session: false}),
  function (req, res, next) {

    const token = getToken(req.headers)
    if (token) {
      let currentUser = ''
      if (req.user !== undefined && req.user !== null) {
        currentUser = req.user.username
      }

      let updDocument = req.body
      // make sure proper user ID is used
      updDocument.userId = currentUser

      // TODO
      // make sure all the photos linked in the doc are from the user
      // make sure the doc if it exists belongs to the user

      //console.log('Update filigrane ' + JSON.stringify(req.body) );
      Document.findOneAndUpdate({documentId: req.params.documentId}, updDocument,
        function (err, post) {
          if (err) return next(err)
          res.json(post)
        })

    } else {
      res.status(403).send({success: false, msg: 'Unauthorized.'})
      return next(err)
    }
  })

/* DELETE DOCUMENT */
router.delete('/:documentId', passport.authenticate('jwt', {session: false}),
  function (req, res, next) {
    const token = getToken(req.headers)
    if (token) {
      let currentUser = ''
      if (req.user !== undefined && req.user !== null) {
        currentUser = req.user.username
      }
      Document.findOneAndRemove({documentId: req.params.documentId}, req.body, function (err, post) {
        if (err) return//next(err);
        res.json(post)
      })
    } else {
      res.status(403).send({success: false, msg: 'Unauthorized.'})
      //next(err);
    }
  })

//
//
//
//

function getToken (headers) {
  if (headers && headers.authorization) {
    const parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

module.exports = router
