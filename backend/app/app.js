'use strict'

let fs = require('fs')
let path = require('path')
let http = require('http')
const https = require('https')

const cors = require("cors");

let express = require('express')
let app = express()

let config = require('./config/config.js')[process.env.NODE_ENV ||
'development']

let logger = require('morgan')

let bodyParser = require('body-parser')

// Create a connection to MongoDB
let mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
let dbconfig = require('./config/database')
mongoose.connect(dbconfig.database, {promiseLibrary: require('bluebird'), useNewUrlParser: true}).
  then(() => console.log('mongodb connection succesful')).
  catch((err) => console.error('mongodb error' + err))

//
//
let filigrane = require('./routes/filigrane')
let user = require('./routes/user')
let image = require('./routes/image')
let thumbnail = require('./routes/thumbnail')
let match = require('./routes/match')

// init passport for route
let passport = require('passport')
app.use(passport.initialize())

app.use(logger('dev'))
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({'limit': '50mb', 'extended': 'false'}))
app.use(express.static(path.join(__dirname, 'dist')))

// enable CORS
// app.use(cors());

app.use(function (req, res, next) {
  console.log ("setup CORS")
  res.header("Access-Control-Allow-Origin", '*');
//  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Authorization, Accept');
  next()
})

app.use('/filigranes', express.static(path.join(__dirname, 'dist')))
app.use('/api/filigrane', filigrane)
app.use('/api/user', user)
app.use('/api/image', image)
app.use('/api/thumbnail', thumbnail)
app.use('/api/match', match)



/*
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
//  res.render('error');
});
*/

// start http server
if (config.is_secure) {
  const port = process.env.NODE_PORT || config.port || 15000
  let server = http.createServer(app).listen(port)
  // start https server
  var sslOptions = {
    key: fs.readFileSync('ssl/localhost.key'),
    cert: fs.readFileSync('ssl/localhost.crt'),
  }
  https.createServer(sslOptions, app).listen(8443)
  console.log('Your server is listening on port %d (http://localhost:%d)',
    config.port, config.port)
} else {
  // Start the server
  http.createServer(app).listen(config.port, function () {
    console.log('Your server is listening on port %d (http://localhost:%d)',
      config.port, config.port)
  })
}
module.exports = app

