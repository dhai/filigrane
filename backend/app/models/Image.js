//
//
//

const mongoose = require('mongoose')

//
//
//

const ImageSchema = new mongoose.Schema({
  imageId: String,
  dataImg: String,
  imageWidth: Number,
  imageHeight: Number,
})

//
//
//

module.exports = mongoose.model('image', ImageSchema)


