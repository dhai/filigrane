//
//
//

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//
//
//

const ParameterSchema = {
  category: String,
  value: String,
  name: String,
}

const ThumabnailSchema = {
  imageId: String,
  imageWidth: Number,
  imageHeight: Number,
}

const PageSchema = {
  pageId: String,
  pageNumber: Number,

  writingOrigin: String,
  writingDate: String,

  documentWidth: Number,
  documentHeight: Number,
  watermarkWidth: Number,
  watermarkHeight: Number,

  thumbnails: {
    type: [ThumabnailSchema],
    unique: false,
    index: false,
    required: false,
  },
  parameters: {
    type: [ParameterSchema],
    unique: false,
    index: false,
    required: false,
  },
}



const DocumentSchema = new mongoose.Schema({
  userId: String,
  documentId: String,

  institution: String,

  documentType: String,

  documentWidth: Number,
  documentHeight: Number,
  watermarkWidth: Number,
  watermarkHeight: Number,
  watermarkShape: String,

  totalPages: Number,

  description: String,
  keywords: String,

  TPQ: Number,
  TAQ: Number,

  allowPhotoRepro: Boolean,
  allowPhotoTrain: Boolean,

  pages: {
    type: [PageSchema],
    unique: false,
    index: false,
    required: false,
  },

  parameters: {
    type: [ParameterSchema],
    unique: false,
    index: false,
    required: false,
  },

  updatedDate: {type: Date, default: Date.now},

})

//
//
//

//module.exports = mongoose.model('document', DocumentSchema);
module.exports = {
  Document: mongoose.model('document', DocumentSchema),
  Page: mongoose.model('page', PageSchema),
}
