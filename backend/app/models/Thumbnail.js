//
//
//

const mongoose = require('mongoose')

//
//
//

const ThumbnailSchema = new mongoose.Schema({
  imageId: String,
  dataImg: String,
  imageWidth: Number,
  imageHeight: Number,
})

//
//
//

module.exports = mongoose.model('thumbnail', ThumbnailSchema)


