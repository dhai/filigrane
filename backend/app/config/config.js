/**
 * New node file
 */

module.exports = {
  'development': {
    swagger_file: 'swagger_development.yaml',

    config_name: 'development',
    port: 9200,

    is_secure: false,

    search_address : "pgia.paris.inria.fr",
    search_port : "5003",
    search_path_photo : "/fpt/api/search_photo",
    search_path_drawing : "/fpt/api/search_drawing",


    // debug parameters
    debug: true,
    log_level_file: 'verbose',
    log_level_console: 'verbose',

    // new user registration or not
    enable_registration: false,
  },
  'ci': {
    swagger_file: 'swagger_development.yaml',

    config_name: 'ci',
    port: 9200,

    is_secure: false,

    search_address : "127.0.0.1",
    search_port : "5002",
    search_path_photo : "/fpt/api/search_photo",
    search_path_drawing : "/fpt/api/search_drawing",

    // debug parameters
    debug: true,
    log_level_file: 'verbose',
    log_level_console: 'verbose',

    // new user registration or not
    enable_registration: false,
  },
  'production': {
    swagger_file: 'swagger_production.yaml',

    config_name: 'production',
    port: 9200,

    is_secure: false,

    search_address : "pgia.paris.inria.fr",
    search_port : "80",
    search_path_photo : "/fpt/api/search_photo",
    search_path_drawing : "/fpt/api/search_drawing",

    // debug parameters
    debug: true,
    log_level_file: 'debug',
    log_level_console: 'verbose',

    // new user registration or not
    enable_registration: false,
  },
}
