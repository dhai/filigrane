/**
 *
 *
 *
 */

const jwt = require('jsonwebtoken')


/**
 *
 */
module.exports = {
  getToken: getToken,
};


/**
 * Retrieves token from message headers.
 * Assumes second part of the authorization header
 *
 * @param headers
 * @returns {*}
 */
function getToken (headers) {
  if (headers && headers.authorization) {
    const parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}


